<?php

class Core_Model_Authlogs extends Zend_Db_Table_Abstract
{
    protected $_name = 'authlogs';

    public function init() {

    }

    public function write($user_id)
    {
        $row = $this->createRow();
        $row->user_id = (int)$user_id;
        $row->date = date('Y-m-d H:i:s');
        $row->save();
    }

    public function getAll()
    {
        $select = $this->select()->from($this->_name, array('date'))->setIntegrityCheck(false)
                ->joinLeft('users', 'users.id = ' . $this->_name . '.user_id', array('login', 'name'))
                ->order($this->_name . '.id DESC')
                ->limit(30);
        return $this->fetchAll($select);
    }

}

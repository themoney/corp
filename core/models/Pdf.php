<?php

/*
  Usage:
  
  class Custom_Pdf_Generator extends Pdf_Generator {
    public drawEntityInRow($order) {
      $this->drawRow(array(
        $order->getId(),
        $order->getCreatedDate(),
        ...
      ));
    }
  }
  
  $pdf = new Custom_Pdf_Generator();
  $pdf->cellWidths = array(
    30,  // Purchased on
    95,  // Order #
    135, // PO #
    185, // Company
    255, // City
    285, // State
    320, // Country
    360, // ZIP
    390, // Order amount
    450, // Commission
    510, // Balance due
    600  // Right margin of the page
  );
  $pdf->tableHeader = array(
    'Purchased on', 'Order #', 'PO #',
    'Company', 'City', 'State', 'Country', 'ZIP',
    'Order amount', 'Commission', 'Balance due');
  $pdf->getTotalsFrom = 'commission';
  
  $items = Mage::getModel('order')->getItems();
  $pdf->render($items);
  // or from controller:
  $pdf->renderAndSend($items, 'example.pdf', $this->response);
*/

class Core_Model_Pdf
{

    protected $pdf, $page, $font, $alt = 0, $y = 570;
    public $fsize = 9;
    public $docTitle = 'Document title';
    public $docSubtitle = 'Subtitle';
    public $getTotalsFrom = 0;
    public $cellWidths = array();
    public $tableHeader = array();


    public function __construct()
    {
        $this->pdf = new Zend_Pdf();
        $this->page = $this->pdf->newPage(Zend_Pdf_Page::SIZE_A4_LANDSCAPE);
        $this->pdf->pages[] = $this->page;
        $this->font = $this->setRegularFont();
    }

    public function render($data, $header)
    {
        $this->drawPageHeader($header);
        $this->drawTableHeading();
        $this->drawRows($data);
        #$this->drawFooter();
        return $this->pdf->render();
    }

    public function renderAndSend($data, $header, $fileName, $response)
    {
        $content = $this->render($data, $header);
        $response->setHeader('HTTP/1.1 200 OK', '');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        // Replace 'inline' with 'attachment' if you wish for browser to force download
        $response->setHeader('Content-Disposition', 'inline; filename=' . $fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', '       application/x-pdf');
        $response->setBody($content);
        $response->sendResponse();
        die;
    }

    private function drawPageHeader($header)
    {
        $font = $this->setBoldFont(12);
        $this->page->setFont($font, 12);
        // header logo
        $logo1 = Zend_Pdf_Image::imageWithPath(APPLICATION_PATH . '/data/logo1.png');
        $logo2 = Zend_Pdf_Image::imageWithPath(APPLICATION_PATH . '/data/logo2.png');
        #$this->page->drawImage($logo1, 25, 780, ($logo1->getPixelWidth()+25), (780+$logo2->getPixelHeight()));
        #$this->page->drawImage($logo2, 335, 780, ($logo2->getPixelWidth()+335), (780+$logo2->getPixelHeight()));
        #$this->page->drawText('Аналитическая система маршрутных листов', 180, 765, 'UTF-8');
        $this->page->drawImage($logo1, 25, 530, ($logo1->getPixelWidth()+25), (530+$logo1->getPixelHeight()));
        $this->page->drawImage($logo2, 590, 530, ($logo2->getPixelWidth()+590), (530+$logo2->getPixelHeight()));
        $this->page->drawText('Сводный отчет по Маршрутным Листам', 325, 565, 'UTF-8');
        $this->page->setFont($this->setBoldFont(9), 9);
        $this->page->drawText('За период: ' . $header['from'] . ' - ' . $header['to'], 325, 550, 'UTF-8');
        $y = 535;
        $x = 325;
        if(!empty($header['type'])) {
            $header['type'] = $header['type']=='Н' ? 'Новые' : 'Рабочие';
            $this->page->drawText($header['type'], $x, $y, 'UTF-8');
            $x += 100;
        }
        if(!empty($header['city'])) {
            $this->page->drawText($header['city'], $x, $y, 'UTF-8');
            $x += 100;
        }
        if(!empty($header['zone'])) {
            $this->page->drawText($header['zone'], $x, $y, 'UTF-8');
            $x += 100;
        }
        if(!empty($header['type']) || !empty($header['city']) || !empty($header['zone'])) $y -= 15;
        if(!empty($header['client'])) {
            $this->page->drawText(stripcslashes($header['client']), 325, $y, 'UTF-8');
        }
        $this->y -= 60;
    }

    protected function insertPage($drawHeader = true)
    {
        $pages = $this->pdf->pages;

        $pages_count = count($pages);
        if ($pages_count == 1) {
            $this->setBoldFont($this->fsize + 1);
            $pages[0]->drawText(1, 450, 10);
            $this->setRegularFont();
        }
        $this->page = $this->pdf->newPage(Zend_Pdf_Page::SIZE_A4_LANDSCAPE);
        $this->pdf->pages[] = $this->page;

        $this->y = 580;
        if ($drawHeader) $this->drawTableHeading();
        $this->setBoldFont($this->fsize + 1);
        $this->page->drawText($pages_count + 1, 450, 10);
        $this->setRegularFont();
        return $this->page;
    }

    protected function drawRow($cells)
    {
        $x = $this->cellWidths;
        $i = 0;
        while ($i <= 10) $this->drawTextInCell($cells[$i], $x[$i], $x[++$i]);
    }

    protected function drawTableHeading()
    {
        $this->page->setFont($this->font, $this->fsize);
        $this->setBoldFont($this->fsize + 1);
        $this->page->setFillColor(new Zend_Pdf_Color_RGB(0.93, 0.92, 0.92));
        $this->page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
        $this->page->setLineWidth(0.5);
        $this->page->drawRectangle(25, $this->y, 825, $this->y - 15);
        $this->y -= 10;
        $this->page->setFillColor(new Zend_Pdf_Color_RGB(0.4, 0.4, 0.4));
        $this->drawRow($this->tableHeader);
        $this->page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $this->setRegularFont();
        $this->y -= 20;
    }

    /** Make sure you overwrite this method **/
    public function drawEntityInRow($entity)
    {
        if($this->alt % 2) {
            $this->page->setFillColor(new Zend_Pdf_Color_Html('black'))->setAlpha(0.2);
            $this->page->drawRectangle(25, $this->y - 3, 825, $this->y + 9, Zend_Pdf_Page::FILL_METHOD_EVEN_ODD);
            $this->page->setFillColor(new Zend_Pdf_Color_GrayScale(0))->setAlpha(1);
        } else {
            #$this->page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        }
        $this->drawRow($entity);
        $this->alt++;
        #$this->page->drawLine(25, $this->y - 3, 825, $this->y - 3);
        #$this->y -= 5;
    }

    /* Feel free to overwrite */
    public function drawRows($entities)
    {
        $total = 0;
        foreach ($entities as $id => $entity) {
            $this->drawEntityInRow($entity);
            #$total += $entity[$this->getTotalsFrom];
            $this->y -= 13;
            if ($this->shouldInsertPage()) $this->insertPage();
        }
        #$this->drawTotals($total);
        $this->setItalicFont($this->fsize + 1);
        $this->page->drawLine(25, $this->y + 5, 825, $this->y + 5);
        $this->y -= 10;
        $this->drawText(date('Отчёт создан d.m.Y в H:i'), 670, $this->y);
        $this->setRegularFont();
    }

    protected function drawTotals($total)
    {
        $this->page->drawLine(25, $this->y + 7, 570, $this->y + 7);
        $this->y -= 3;
        $this->setBoldFont($this->fsize + 1);
        $this->drawText($total, 450);
        $this->setRegularFont();
    }

    protected function shouldInsertPage()
    {
        return $this->y < 20;
    }

    protected function drawText($text, $x, $y = -1)
    {
        if(!is_numeric($x)) $x = 0;
        $this->page->drawText($text, $x, (($y < 0) ? $this->y : $y), 'UTF-8');
    }

    protected function drawTextInCell($text, $x1, $x2, $y = -1, $font = null, $fontSize = null)
    {
        if (!$font)
            $font = $this->font;
        if (!$fontSize)
            $fontSize = $this->fsize;

        $width = $this->widthForString($text, $font, $fontSize);
        $charWidth = $this->widthForString('w', $font, $fontSize);
        if ($width > $x2 - $x1) {
            $widthS = $this->widthForString($text, $font, $fontSize - 1);
            $this->page->setFont($font, $fontSize - 1);
            // Could need some love:
            #if ($widthS > $x2 - $x1)
                #$text = substr((string)$text, 0, -ceil(($widthS - $x2 - $x1) / $charWidth)) . '...';
        }

        $this->drawText($text, $x1, $y);
        if (isset($widthS))
            $this->page->setFont($font, $fontSize);
    }

    public function widthForString($string, $font, $fontSize)
    {
        #$drawingString = iconv('UTF-8', 'UTF-16BE//IGNORE', $string);
        $drawingString = $string;
        $strlen_drawingString = strlen($drawingString);
        #$characters = array();
        #for ($i = 0; $i < $strlen_drawingString; $i++)
        #    $characters[] = (ord($drawingString[$i++]) << 8) | ord($drawingString[$i]);
        $stringWidth = $strlen_drawingString;
        #$glyphs = $font->glyphNumbersForCharacters($characters);
        #$widths = $font->widthsForGlyphs($glyphs);
        #$stringWidth = (array_sum($widths) / $font->getUnitsPerEm()) * $fontSize;
        return $stringWidth;
    }

    protected function setRegularFont($size = -1)
    {
        $font = Zend_Pdf_Font::fontWithPath(APPLICATION_PATH . '/data/font.ttf');
        $this->page->setFont($font, $size < 0 ? $this->fsize : $size);
        return $font;
    }

    protected function setBoldFont($size = -1)
    {
        $font = Zend_Pdf_Font::fontWithPath(APPLICATION_PATH . '/data/font-b.ttf');
        $this->page->setFont($font, $size < 0 ? $this->fsize : $size);
        return $font;
    }

    protected function setItalicFont($size = -1)
    {
        $font = Zend_Pdf_Font::fontWithPath(APPLICATION_PATH . '/data/font-i.ttf');
        $this->page->setFont($font, $size < 0 ? $this->fsize : $size);
        return $font;
    }
}
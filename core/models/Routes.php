<?php

class Core_Model_Routes extends Zend_Db_Table_Abstract
{
    protected $_name = 'routes';

    public function init()
    {

    }

    public function getCalendar($start, $end, $users, $user = false)
    {
        /*$start = date('Y-m-d', $start);
        $end = date('Y-m-d', $end);
        #echo $start . " - " . $end . "<br />";
        #$subselect = $this->select()->from('tasks_users', 'task_id')->setIntegrityCheck(false);
        #if(is_numeric($user_id)) { $subselect->where('user_id = ?', (int)$user_id); }
        $select = $this->select()->from($this->_name)->setIntegrityCheck(false)
            ->where('(date BETWEEN ' . $this->getAdapter()->quote($start) . ' AND ' . $this->getAdapter()->quote($end) . ')');
            #->where('id IN ?', $subselect);
        if(is_numeric($user_id)) {
            $select->joinLeft('users', 'users.id = ' . $this->_name . '.zone', array('group_id', 'name', 'city'))
            ->where('users.id = ?', (int)$user_id);
        }
        exit($select->__toString());
        return $this->fetchAll($select);*/

        #$subselect = $this->select()->from('users', 'city')->where('users.id = routes.zone')->limit(1)->setIntegrityCheck(false);
        #->joinLeft('users', 'users.id = ' . $this->_name . '.zone', array('city'))

        $start = date('Y-m-d', $start);
        $end = date('Y-m-d', $end);
        $select = $this->select()->from($this->_name, array('count' => new Zend_Db_Expr('COUNT(*)'), 'time_visit', 'zone', 'date'))
                ->setIntegrityCheck(false)
                ->joinLeft('users', 'users.id = ' . $this->_name . '.zone', array('name', 'city'))
                ->where('date BETWEEN ' . $this->getAdapter()->quote($start) . ' AND ' . $this->getAdapter()->quote($end))
                ->group('date')
                ->group('zone')
                ->group('time_visit');

        # Определяем группу и его права
        switch($user->group_id) {

            case 1:
                # Начальник отдела продаж
                # Доступно всё
                $select->where('zone IN (?) OR user_id IN (?)', $users);
            break;

            case 2:
                # Супервайзер
                # - Доступно всё своего города
                # - Доступно всё личное
                $usersModel = new Core_Model_Users();
                $city = $usersModel->getCity($user->id);
                $select->where("routes.city = '" . $city . "'", (int)$user->id);
                $select->where('zone IN (?) OR user_id IN (?)', $users);
                #exit($select->__toString());
            break;

            default:
                # Торговый
                # - Доступно всё личное
                $select->where('zone = ? OR user_id = ?', (int)$user->id);
            break;
        }
        #exit($select->__toString());
        return $this->fetchAll($select);
    }

    public function getRoute($date, $zone)
    {
        $start = date('Y-m-d', $date);
        $select = $this->select()->where('date = ?', $start)->where('zone = ?', $zone);
        return $this->fetchAll($select);
    }

}
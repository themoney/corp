<?php

class Core_Model_Clients extends Zend_Db_Table_Abstract
{
    protected $_name = 'clients';

    public function init() {

    }

    public function add($term)
    {
        $row = $this->fetchRow($this->select()->where("name = ?", $term));
        if(is_null($row) || empty($row->id)) {
            $row = $this->createRow();
            $row->name = $term;
            return $row->save();
        }
        return $row->id;
    }

    public function autocomplete($term)
    {
        $select = $this->select()->where("name LIKE ?", "%" . $term . "%")->limit(10);
        $rowset = $this->fetchAll($select);
        $result = array();
        foreach($rowset as $row) {
            $result[] = array('id' => $row->id, 'label' => $row->name, 'value' => $row->name);
        }
        return $result;
    }

}

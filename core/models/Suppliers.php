<?php

class Core_Model_Suppliers extends Zend_Db_Table_Abstract
{
    protected $_name = 'm_suppliers';

    public function init()
    {

    }

    public function add($id, $name)
    {
        $row = $this->fetchRow($this->select()->where("id = ?", (int)$id));
        if(is_null($row) || empty($row->id) || $row->name != $name) {
            if(!is_null($row) && $row->name != $name) { $row->delete(); }
            $row = $this->createRow();
            $row->id = (int)$id;
            $row->name = $name;
            return $row->save();
        }
        return $row->id;
    }
    
}
<?php

class Core_Model_Fuel extends Zend_Db_Table_Abstract
{
    protected $_name = 'fuel';

    public function init()
    {

    }

    public function getCalendar($user)
    {
        $select = $this->select()->where('user_id = ?', $user->id)
            ->where('DATE_FORMAT(starttime, "%Y-%m-%d") = DATE_FORMAT(NOW(), "%Y-%m-%d")');
        #exit($select->__toString());
        $row = $this->fetchRow($select);
        if($row == null) {
            // ещё не отмечено начало дня
            return 1;
        } else if(!$row->end) {
            // не отмечен конец дня, известен id
            return $row->id;
        } else {
            // всё уже отмечено
            return 0;
        }
    }

    public function getReport($from, $to, $zone)
    {
        /* уточнить по запросу
            SELECT AVG(`start`) AS 'start', FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(`starttime`))) AS 'starttime', AVG(`end`) AS 'end', FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(`endtime`))) AS 'endtime', (SELECT AVG((`end`-`start`)) FROM `fuel` GROUP BY MONTH(`starttime`)) AS 'month', (SELECT AVG((`end`-`start`)) FROM `fuel` GROUP BY DAY(`starttime`)) AS 'day' FROM `fuel` GROUP BY `user_id`
         */
        /*$select = $this->select()->from($this->_name, array('count' => new Zend_Db_Expr('COUNT(*)')))
            ->group('DATE_FORMAT(starttime, "%Y-%m-%d")');*/
        #if(!empty($zone)) $select->where('user_id = ?', (int)$zone);
        #exit($select->__toString());
        $month = "SELECT AVG((`end`-`start`)) FROM `fuel` AS mt WHERE mt.`user_id` = m1.`user_id` GROUP BY MONTH(`starttime`)";
        $day = "SELECT AVG((`end`-`start`)) FROM `fuel` AS dt WHERE dt.`user_id` = m1.`user_id` GROUP BY DAY(`starttime`)";
        #exit("SELECT m1.`user_id`, AVG(m1.`start`) AS 'start', FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(m1.`starttime`))) AS 'starttime', AVG(m1.`end`) AS 'end', FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(m1.`endtime`))) AS 'endtime', (" . $month . ") AS 'month', (" . $day . ") AS 'day' FROM `fuel` AS m1 " . (!empty($zone) ? 'WHERE m1.`user_id` = ' . (int)$zone : '') . " GROUP BY m1.`user_id`");
        $stmt = $this->getAdapter()->query("SELECT m1.`user_id`, AVG(m1.`start`) AS 'start', FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(m1.`starttime`))) AS 'starttime', AVG(m1.`end`) AS 'end', FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(m1.`endtime`))) AS 'endtime', (" . $month . ") AS 'month', (" . $day . ") AS 'day' FROM `fuel` AS m1 " . (!empty($zone) ? 'WHERE m1.`user_id` = ' . (int)$zone : '') . " GROUP BY m1.`user_id`");
        #$stmt = $this->getAdapter()->query("SELECT m1.`user_id`, AVG(m1.`start`) AS 'start', FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(m1.`starttime`))) AS 'starttime', AVG(m1.`end`) AS 'end', FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(m1.`endtime`))) AS 'endtime', (SELECT AVG((`end`-`start`)) FROM `fuel` AS mt WHERE mt.`user_id` = m1.`user_id` GROUP BY MONTH(`starttime`)) AS 'month', (SELECT AVG((`end`-`start`)) FROM `fuel` AS dt WHERE dt.`user_id` = m1.`user_id` GROUP BY DAY(`starttime`)) AS 'day' FROM `fuel` AS m1 GROUP BY m1.`user_id`");
        return $stmt->fetchAll();
    }

}
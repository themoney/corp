<?php

class Core_Model_Templates extends Zend_Db_Table_Abstract
{
    protected $_name = 'templates';

    public function init()
    {

    }

    public function getView($user = false)
    {
        /*$start = date('Y-m-d', $start);
        $end = date('Y-m-d', $end);
        #echo $start . " - " . $end . "<br />";
        #$subselect = $this->select()->from('tasks_users', 'task_id')->setIntegrityCheck(false);
        #if(is_numeric($user_id)) { $subselect->where('user_id = ?', (int)$user_id); }
        $select = $this->select()->from($this->_name)->setIntegrityCheck(false)
            ->where('(date BETWEEN ' . $this->getAdapter()->quote($start) . ' AND ' . $this->getAdapter()->quote($end) . ')');
            #->where('id IN ?', $subselect);
        if(is_numeric($user_id)) {
            $select->joinLeft('users', 'users.id = ' . $this->_name . '.zone', array('group_id', 'name', 'city'))
            ->where('users.id = ?', (int)$user_id);
        }
        exit($select->__toString());
        return $this->fetchAll($select);*/

        #$subselect = $this->select()->from('users', 'city')->where('users.id = routes.zone')->limit(1)->setIntegrityCheck(false);
        #->joinLeft('users', 'users.id = ' . $this->_name . '.zone', array('city'))

        $select = $this->select()->from($this->_name, array('count' => new Zend_Db_Expr('COUNT(*)'), 'zone', 'day_week'))
                ->setIntegrityCheck(false)
                ->joinLeft('users', 'users.id = ' . $this->_name . '.zone', array('name', 'city'))
                ->group('day_week')
                ->group('zone');

        # Определяем группу и его права
        switch($user->group_id) {

            case 1:
                # Начальник отдела продаж
                # Доступно всё
            break;

            case 2:
                # Супервайзер
                # - Доступно всё своего города
                # - Доступно всё личное
                $users = new Core_Model_Users();
                $city = $users->getCity($user->id);
                $select->where("zone = ? OR user_id = ? OR templates.city = '" . $city . "'", (int)$user->id);
                #exit($select->__toString());
            break;

            default:
                # Торговый
                # - Доступно всё личное
                $select->where('zone = ? OR user_id = ?', (int)$user->id);
            break;
        }
        return $this->fetchAll($select);
    }

    public function getTemplate($day_week, $zone = null)
    {
        $select = $this->select()->where('day_week = ?', (int)$day_week);
        if(!is_null($zone)){ $select->where('zone = ?', (int)$zone); }
        return $this->fetchAll($select);
    }

}
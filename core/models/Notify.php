<?php

class Core_Model_Notify extends Zend_Db_Table_Abstract
{
    protected $_name = 'notify';

    public function init() {
                
    }

    public function getUnread($user_id) {
        $select = $this->select()->from($this->_name, array('count' => new Zend_Db_Expr('COUNT(*)')))->where('user_id = ?', (int)$user_id)->where('`read` IS NULL');
        return $this->fetchRow($select);
    }

   public function getAll($user_id) {
        return $this->fetchAll('user_id = ' . (int)$user_id, 'id DESC', 30);
    }

    public function add($user_id, $text)
    {
        $row = $this->createRow();
        $row->user_id = (int)$user_id;
        $row->text = $text;
        $row->time = date('Y-m-d H:i:s');
        $row->save();
    }
}
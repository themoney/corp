<?php

class Core_Model_Files extends Zend_Db_Table_Abstract
{
    protected $_name = 'files';

    public function init() {
                
    }

   public function getAll($user_id) {
        $select = $this->select()->from($this->_name)
                ->where('user_id = ?', (int)$user_id)
                ->orWhere('(SELECT count(`id`) FROM `files_users` WHERE `file_id` = `files`.`id` AND `files_users`.`user_id` = ?)', (int)$user_id)
                ->order('id DESC')
                ->setIntegrityCheck(false)
                ->joinLeft('users', 'users.id = ' . $this->_name . '.user_id', array('name'))
                ->limit(30);
        #exit($select->__toString());
        return $this->fetchAll($select);
    }

    public function getFile($filename) {
        return $this->fetchRow($this->getAdapter()->quoteInto('filename = ?', $filename));
    }

    public function add($user_id, $title, $size, $mime, $filename)
    {
        $row = $this->createRow();
        $row->user_id = (int)$user_id;
        $row->title = $title;
        $row->size = $size;
        $row->mimetype = $mime;
        $row->filename = $filename;
        $row->date = date('Y-m-d H:i:s');
        return $row->save();
    }

    public function remove($id) {
        $this->find($id)->current()->delete();
    }

}
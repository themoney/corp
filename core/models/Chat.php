<?php

class Core_Model_Chat extends Zend_Db_Table_Abstract
{
    protected $_name = 'chat_log';

    public function init() {
                
    }

    public function getPM($user_id, $from, $last = 0) {
        $select = $this->select()->from($this->_name, array('id', 'text', 'date'))
                ->setIntegrityCheck(false)
                ->joinLeft('users', 'users.id = ' . $this->_name . '.from', array('name'))
                ->order($this->_name . '.id DESC')
                ->where('`to` IN(?) AND `from` IN(?)', array((int)$user_id, (int)$from))
                ->where(new Zend_Db_Expr('`to` IS NOT NULL'))
                ->limit(20);
        if((int)$last > 0) { $select->where($this->_name . '.id > ?', (int)$last); $this->update(array('read' => 1), '`to` = ' . (int)$user_id . ' AND `from` = ' . (int)$from . ' AND `read` IS NULL AND `id` <= ' . (int)$last); }
        #exit($select->__toString());
        return $this->fetchAll($select);
    }

   public function getAll($last = 0) {
       $select = $this->select()->from($this->_name, array('id', 'text', 'date'))
               ->setIntegrityCheck(false)
               ->joinLeft('users', 'users.id = ' . $this->_name . '.from', array('name'))
               ->where(new Zend_Db_Expr('`to` IS NULL'))
               ->order($this->_name . '.id DESC')
               ->limit(20);
       if((int)$last > 0) { $select->where($this->_name . '.id > ?', (int)$last); }
       #exit($select->__toString());
       return $this->fetchAll($select);
    }

    public function add($from, $text, $to = 0)
    {
        $row = $this->createRow();
        $row->from = (int)$from;
        $row->text = $text;
        if($to > 0) {
            #$notify = new Core_Model_Notify();
            #$users = new Core_Model_Users();
            #$user = $users->find((int)$from)->current();
            #$notify->add((int)$to, 'Личное сообщение от <b>' . $user->name . '</b> (<a href="javascript:Core.PM(' . (int)$from . ');">посмотреть</a>)');
            $row->to = (int)$to;
        }
        $row->save();
    }
}
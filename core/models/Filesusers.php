<?php

class Core_Model_Filesusers extends Zend_Db_Table_Abstract
{
    protected $_name = 'files_users';

	protected $_referenceMap = array(
	'File' => array(
			'columns'		=> array('file_id'),
			'refTableClass' => 'Core_Model_Files',
			'refColumns'	=> array('id')
	),
	'User' => array(
			'columns'		=> array('user_id'),
			'refTableClass' => 'Core_Model_Users',
			'refColumns'	=> array('id')
	));

    public function init()
    {

    }

    public function getStatus($file_id = null, $user_id = null)
    {
        $result = array();
        $tasks = $this->fetchAll('file_id = ' . (int)$file_id);
        foreach($tasks as $task) {
            $result[$task->user_id] = $task->status;
        }
        return $result;
    }
}
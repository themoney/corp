<?php

class Core_Model_Tasks extends Zend_Db_Table_Abstract
{
    protected $_name = 'tasks';

    protected $_dependentTables = array('Core_Model_Tasksusers');

    public function init()
    {

    }

    public function getCalendar($start, $end, $users)
    {
        $start = date('Y-m-d H:i:s', $start);
        $end = date('Y-m-d H:i:s', $end);
        #echo $start . " - " . $end . "<br />";
        #$subselect = $this->select()->from('tasks_users', 'task_id')->setIntegrityCheck(false);
        #if(is_numeric($user_id)) { $subselect->where('user_id = ?', (int)$user_id); }
        $select = $this->select()->from($this->_name)->setIntegrityCheck(false)
            ->joinLeft('tasks_users', 'tasks_users.task_id = ' . $this->_name . '.id', array('user_status' => 'status'))
            ->where('(start BETWEEN ' . $this->getAdapter()->quote($start) . ' AND ' . $this->getAdapter()->quote($end) . ') OR (end BETWEEN ' . $this->getAdapter()->quote($start) . ' AND ' . $this->getAdapter()->quote($end) . ')')
            ->group('tasks_users.task_id');
        $select->where('tasks.user_id IN (?) OR tasks_users.user_id IN (?)', $users);
        #exit($select->__toString());
        return $this->fetchAll($select);
    }
}
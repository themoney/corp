<?php

class Core_Model_Tasksusers extends Zend_Db_Table_Abstract
{
    protected $_name = 'tasks_users';

	protected $_referenceMap = array(
	'Task' => array(
			'columns'		=> array('task_id'),
			'refTableClass' => 'Core_Model_Tasks',
			'refColumns'	=> array('id')
	),
	'User' => array(
			'columns'		=> array('user_id'),
			'refTableClass' => 'Core_Model_Users',
			'refColumns'	=> array('id')
	));

    public function init()
    {

    }

    public function getStatus($task_id = null, $user_id = null)
    {
        $result = array();
        $tasks = $this->fetchAll('task_id = ' . (int)$task_id);
        foreach($tasks as $task) {
            $result[$task->user_id] = $task->status;
        }
        return $result;
    }
}
<?php

class Core_Model_Reports extends Zend_Db_Table_Abstract
{
    protected $_name = 'routes';

    public function init()
    {

    }

    public function getReport($from = false, $to = false, $type = false, $city = false, $client = false, $zone = false)
    {
        $select = $this->select()
                ->from($this->_name, array('client', 'address', 'zone', 'count' => new Zend_Db_Expr('COUNT(*)'), 'mpz' => new Zend_Db_Expr('AVG(mpz)'), 'prochka' => new Zend_Db_Expr('AVG(proch)')))
                ->group(array('client','address'));
        if(!empty($from)) $select->where('date >= ?', $from);
        if(!empty($to)) $select->where('date <= ?', $to);
        if(!empty($type)) $select->where('hp = ?', $type);
        if(!empty($city)) $select->where('city = ?', $city);
        if(!empty($client)) $select->where('client = ?', $client);
        if(!empty($zone)) $select->where('zone = ?', (int)$zone);
        #exit($select->__toString());
        return $this->fetchAll($select);
    }

}
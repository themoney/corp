<?php

class Core_Model_Groups extends Zend_Db_Table_Abstract
{
    protected $_name = 'groups';

    

    public function init()
    {

    }

    public function getArray()
    {
        $result = array();
        $rowset = $this->fetchAll();
        foreach($rowset as $row) {
            $result[(int)$row->id] = $row->name;
        }
        return $result;
    }
}
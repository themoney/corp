<?php

class Core_Model_Users extends Zend_Db_Table_Abstract
{
    protected $_name = 'users';

    protected $_dependentTables = array('Core_Model_Tasksusers', 'Core_Model_Groups');

    public function init()
    {

    }

    public function getCity($user_id)
    {
        return $this->find($user_id)->current()->city;
    }

    public function getZones()
    {
        $zone = array();
        $result = $this->fetchAll();
        foreach($result as $row) {
            $zone[$row->id] = $row->name;
        }
        return $zone;
    }

    public function getList($user_id = false)
    {
        $select = $this->select()->from($this->_name)->setIntegrityCheck(false)
            ->joinLeft('groups', 'groups.id = users.group_id', array('group_id' => 'id', 'group' => 'name'))
            ->order('groups.id ASC')
            ->order('users.city ASC')
            ->order('users.name ASC');

        if((int)$user_id > 0) {
            $user = $this->fetchRow('id = ' . (int)$user_id);
            if(is_null($user)) throw new Zend_Exception('Пользователь не найден!');
            switch((int)$user->group_id) {
                case 2:
                    $select->where('users.city = ?', $user->city);
                break;

                case 3:
                    $select->where('users.id = ?', $user->id)->limit(1);
                break;
            }
        }
        return $this->fetchAll($select);
    }

    public function getOnline($user_id)
    {
        $select = $this->select()->from($this->_name, array('id', 'login', 'name', 'city', 'online' => new Zend_Db_Expr('TIMESTAMPDIFF(MINUTE,users.last,NOW())'), 'unread' => new Zend_Db_Expr("(SELECT COUNT(*) FROM `chat_log` WHERE `chat_log`.`to` = " . (int)$user_id . " AND `chat_log`.`from` = `users`.`id` AND `chat_log`.`read` IS NULL)")))
            ->setIntegrityCheck(false)
            ->joinLeft('groups', 'groups.id = users.group_id', array('group_id' => 'id', 'group' => 'name'))
            ->order('groups.id ASC')
            ->order('users.city ASC')
            ->order('users.name ASC');
        #exit($select->__toString());
        return $this->fetchAll($select);
    }
}
<?php

class Core_Model_Monitoring extends Zend_Db_Table_Abstract
{
    protected $_name = 'm_data';

    public function init() {
                
    }

    public function getData($from = '', $to = '', $product = '')
    {
        $select = $this->select()
                    ->from($this->_name, array('supplier_id', 'avg' => new Zend_Db_Expr('AVG(value)'), 'date'))
                    ->group('supplier_id')->group('product_id')->group('date')
                    ->where("supplier_id = ?", 17);
        if(!empty($from)) $select->where("date >= ?", $from);
        if(!empty($to)) $select->where("date <= ?", $to);
        if(!empty($product)) $select->where("product_id = ?", (int)$product);
        #exit($select->__toString());
        return $this->fetchAll($select);
    }

    public function getAllData($from = '', $to = '', $product = '', $supplier = '')
    {
        $select = $this->select()
                    ->from($this->_name, array('supplier_id', 'avg' => new Zend_Db_Expr('AVG(value)'), 'date'))
                    ->group('supplier_id')->group('product_id')->group('date');
        if(!empty($from)) $select->where("date >= ?", $from);
        if(!empty($to)) $select->where("date <= ?", $to);
        if(!empty($product)) $select->where("product_id = ?", (int)$product);
        if(!empty($supplier)) $select->where("supplier_id = ?", (int)$supplier);
        #exit($select->__toString());
        return $this->fetchAll($select);
    }

    public function add($supplier, $product, $value, $date = '')
    {
        $row = $this->createRow();
        $row->supplier_id = (int)$supplier;
        $row->product_id = (int)$product;
        $row->value = (float)$value;
        $row->date = (empty($date) ? date('Y-m-d') : $date);
        return $row->save();
    }

}
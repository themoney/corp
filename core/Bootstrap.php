<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    protected function _initSession()
    {
        Zend_Session::start();
        #Zend_Session::rememberMe(3600);
        Zend_Registry::set('session', new Zend_Session_Namespace('Core'));
        #Zend_Wildfire_Plugin_FirePhp::getInstance()->send('Session', 'Init', Zend_Wildfire_Plugin_FirePhp::INFO);
    }

    /*protected function _initConfig()
    {
        Zend_Registry::set('config', new Zend_Config($this->getOptions()));
    }*/

    protected function _initCache()
    {
        $options = $this->getOptions();
        $cache = Zend_Cache::factory(
            $options['cache']['frontend']['adapter'],
            $options['cache']['backend']['adapter'],
            $options['cache']['frontend']['params'],
            $options['cache']['backend']['params']
        );
        if((bool)$options['cache']['isDefaultMetadataCache']) Zend_Db_Table_Abstract::setDefaultMetadataCache($cache);
        if((bool)$options['cache']['isDefaultTranslateCache']) Zend_Translate::setCache($cache);
        if((bool)$options['cache']['isDefaultLocaleCache']) Zend_Locale::setCache($cache);
        if((bool)$options['cache']['isRegistrySet']) Zend_Registry::set('cache', $cache);
        #Zend_Wildfire_Plugin_FirePhp::getInstance()->send('Cache', 'Init', Zend_Wildfire_Plugin_FirePhp::INFO);
        return $cache;
    }

    protected function _initDb()
    {
        try {
            $config = $this->getOptions();
            $db = Zend_Db::factory($config['database']['adapter'], $config['database']['params']);
            Zend_Db_Table::setDefaultAdapter($db);
        } catch (Exception $e) {
            exit($e->getMessage());
        }
        Zend_Registry::set('db', $db);
        #Zend_Wildfire_Plugin_FirePhp::getInstance()->send('MySQL', 'Init', Zend_Wildfire_Plugin_FirePhp::INFO);
        return $db;
    }

    protected function _initAcl() {
        $acl = new Zend_Acl();
        $this->bootstrap('db');
        $db = Zend_Registry::get('db');
        $groups = $db->fetchAll($db->select()->from('groups'));
        # Настраиваем Роли
        $guest = new Zend_Acl_Role('Гость');
        $acl->addRole($guest);
        $grouplist = array();
        foreach($groups as $group) {
            $acl->addRole(new Zend_Acl_Role($group['name']), 'Гость');
            $grouplist[$group['id']] = $group['name'];
        }
        $acl->addResource(new Zend_Acl_Resource('index'));
        $acl->allow('Гость', 'index', array('index','login', 'pass'));
        
        $acl->allow($grouplist[3], 'index');
        $acl->addResource(new Zend_Acl_Resource('chat'));
        $acl->allow($grouplist[3], 'chat');
        $acl->addResource(new Zend_Acl_Resource('task'));
        $acl->allow($grouplist[3], 'task');
        $acl->addResource(new Zend_Acl_Resource('route'));
        $acl->allow($grouplist[3], 'route');
        $acl->addResource(new Zend_Acl_Resource('fuel'));
        $acl->allow($grouplist[3], 'fuel');
        $acl->addResource(new Zend_Acl_Resource('template'));
        $acl->allow($grouplist[3], 'template');
        $acl->addResource(new Zend_Acl_Resource('calendar'));
        $acl->allow($grouplist[3], 'calendar');
        $acl->addResource(new Zend_Acl_Resource('clients'));
        $acl->allow($grouplist[3], 'clients');
        $acl->addResource(new Zend_Acl_Resource('address'));
        $acl->allow($grouplist[3], 'address');
        $acl->addResource(new Zend_Acl_Resource('notify'));
        $acl->allow($grouplist[3], 'notify');
        $acl->addResource(new Zend_Acl_Resource('files'));
        $acl->allow($grouplist[3], 'files');
        $acl->addResource(new Zend_Acl_Resource('monitoring'));
        $acl->allow($grouplist[3], 'monitoring');

        # Разрешить всё
        $acl->addResource(new Zend_Acl_Resource('reports'));
        $acl->allow($grouplist[2]);
        $acl->addResource(new Zend_Acl_Resource('logs'));
        $acl->allow($grouplist[1]);

        #require_once 'Core/Controller/Plugin/Acl.php';
        $aclPlugin = new Core_Controller_Plugin_Acl($acl, 'Гость');
        #$aclPlugin->throwExceptions(true);
        $aclPlugin->denyUnknown(true);
        $frontController = Zend_Controller_Front::getInstance();
        $frontController->registerPlugin($aclPlugin);
        Zend_Registry::set('acl', $aclPlugin);

        #require_once 'Core/Controller/Action/Helper/Acl.php';
        #Zend_Controller_Action_HelperBroker::addHelper(new Core_Controller_Action_Helper_Acl());

        $acl = Zend_Auth::getInstance();
        if($acl->hasIdentity()) {
            $this->bootstrap('layout');
            $layout = $this->getResource('layout');
            $layout->setLayout('layout');
            $auth = Zend_Auth::getInstance();
            $aclPlugin->setRoleName($auth->getIdentity()->group_name);
            $db->update('users', array('last' => new Zend_Db_Expr('NOW()')), 'id = ' . (int)$auth->getIdentity()->id);
        }
    }

    /*protected function _initLogger()
    {
        try {
            $this->bootstrap('db');
            $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH . '/logs/errors.log');
            #$logger = new Zend_Log ($writer);
            /*$columnMapping = array(
                'priority'     => 'priority',
                'priorityName' => 'priorityName',
                'message'      => 'message',
                'timestamp'    => 'timestamp',
                'ip'           => 'ip',
                'controller'   => 'controller',
                'action'       => 'action'
            );

            $message = '';

            $message .= "Server time: " . date("Y-m-d H:i:s") . "\n";
            $message .= "RequestURI: " . $_SERVER['PHP_SELF'] . "?" . $_SERVER['QUERY_STRING'] . "\n";

            $message .= "Message: " . $e->getMessage() . "\n\n";
            $message .= "Trace:\n" . $e->getTraceAsString() . "\n\n";
            $message .= "Request data: " . var_export($_GET, true) . "\n\n";

            $profiler = $db->getProfiler();
            $query = $profiler->getLastQueryProfile()->getQuery();
            $queryParams = $profiler->getLastQueryProfile()->getQueryParams();

            $message .= "Last database query: " . $query . "\n\n";
            $message .= "Last database query params: " . var_export($queryParams, true) . "\n\n";

            $log->emerg($message);

            $writer = new Zend_Log_Writer_Db(Zend_Registry::get('db'), 'logs', $columnMapping);
            #$formatter = new Zend_Log_Formatter_Simple()
            #$writer->setFormatter($formatter);
            $logger = new Zend_Log($writer);
            $logger->registerErrorHandler();
            Zend_Registry::set("log", $logger);
            #Zend_Controller_Action_HelperBroker::addPrefix("Core_Controller_Action_Helper");
        } catch (Exception $e) {
            exit($e->getMessage());
        }
    }*/

    protected function _initTranslator() {
        $translator = new Zend_Translate(
            'array',
            APPLICATION_PATH . '/../resources/languages',
            'ru',
            array('scan' => Zend_Translate::LOCALE_DIRECTORY)
        );
        Zend_Validate_Abstract::setDefaultTranslator($translator);
        Zend_Registry::set('translator', $translator);
        return $translator;
    }

    protected function _initView()
    {
        $config = $this->getOptions();
        $view = new Zend_View();
        #$view->addHelperPath('Core/View/Helper','Core_View_Helper');
        $view->doctype($config['resources']['layout']['doctype']);
        $view->headTitle()->setSeparator($config['resources']['view']['titleSeparator'])->set($config['resources']['view']['title']);
        $view->headMeta()->appendName('description', $config['resources']['view']['description']);
        $view->headMeta()->appendName('keywords', $config['resources']['view']['keywords']);
        $view->headMeta()->appendHttpEquiv('Content-Type', $config['resources']['view']['contentType']);
        $view->headMeta()->appendHttpEquiv('X-UA-Compatible','IE=EmulateIE7');
        $view->headLink()->headLink(array('rel' => 'favicon', 'href' => '/core/images/favicon.ico'), 'PREPEND');
        $view->headLink()->headLink(array('rel' => 'icon', 'type' => 'image/x-icon', 'href' => '/core/images/favicon.ico'), 'PREPEND');
        $view->headLink()->headLink(array('rel' => 'shortcut icon', 'type' => 'image/x-icon', 'href' => '/core/images/favicon.ico'), 'PREPEND');
        #$view->headLink()->appendStylesheet('/core/css/fullcalendar.print.css');
        #$view->headLink()->appendStylesheet('/core/ui-darkness/jquery-ui-1.8.12.custom.css');
        #$view->headLink()->appendStylesheet('/core/black-tie/jquery-ui-1.8.12.custom.css');
        $view->headLink()->appendStylesheet('/core/smoothness/jquery-ui-1.8.12.custom.css');
        $view->headLink()->appendStylesheet('/core/css/fullcalendar.css');
        $view->headLink()->appendStylesheet('/core/css/index.css');
        $view->headScript()->appendFile('/core/js/jquery/jquery-1.6.min.js');
        #$view->headScript()->appendFile("https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js");
        $view->headScript()->appendFile('/core/js/jquery/jquery-ui-1.8.12.custom.min.js');
        #$view->headScript()->appendFile('/core/js/enhance.js');
        #$view->headScript()->appendFile('/core/js/excanvas.js');
        #$view->headScript()->appendFile('/core/js/visualize.jquery.js');
        $view->headScript()->appendFile('/core/js/fullcalendar/fullcalendar.js');
        $view->headScript()->appendFile('/core/js/jquery.favicon.js');
        $view->headScript()->appendFile('/core/js/jquery.history.min.js');
        $view->headScript()->appendFile('/core/js/functions.js');
        $view->headScript()->appendFile('/core/js/core.js');
        #$view->headScript()->appendFile('https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js');
        #$view->headScript()->appendFile('http://yandex.st/jquery/form/2.52/jquery.form.min.js');
        #$view->headScript()->appendFile('/pie/PIE.js');
        #$view->headScript()->appendFile('/core/js/jquery.fixpng.js');
        #$view->headScript()->appendFile('/core/js/jquery.datepicker.js');
        #$view->headScript()->appendFile('/core/js/jquery.pnotify.min.js');
        #$view->headScript()->appendFile('/core/js/system.js');
        #Zend_Wildfire_Plugin_FirePhp::getInstance()->send('View', 'Init', Zend_Wildfire_Plugin_FirePhp::INFO);
        return $view;
    }

    /*protected function __initDevelopmentBar()
    {
    	try {
		    #if(APPLICATION_ENV !== 'development') return;
        	$autoloader = Zend_Loader_Autoloader::getInstance();
	        $autoloader->registerNamespace('Core');
            $frontController = Zend_Controller_Front::getInstance();
            $frontController->registerPlugin(new Core_Controller_Plugin_Developmentbar());
            #Zend_Wildfire_Plugin_FirePhp::getInstance()->send('DevBar', 'Init', Zend_Wildfire_Plugin_FirePhp::INFO);
        } catch (Exception $e) {
            exit($e->getMessage());
        }
    }*/
}

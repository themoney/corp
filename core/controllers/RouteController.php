<?php

class RouteController extends Zend_Controller_Action
{

    protected $_model;

    protected $status = array(0 => 'new', 1 => 'read', 2 => 'completed');

    protected $mark = array(0 => '☐', 1 => '☐', 2 => '☑');

    protected $_acl;

    protected $user;

    public function init()
    {
        /* Initialize action controller here */
        $this->_model = new Core_Model_Routes();
        $this->_acl = Zend_Auth::getInstance();
        $this->user = $this->_acl->getIdentity();
        $this->_helper->layout->disableLayout();
    }

    public function indexAction()
    {
        $request = $this->getRequest();
        $params = $request->getParams();
        $start = $params['start'];#time()-(60*60*24);
        $end = $params['end'];#time()+(60*60*24);
        $users = array_map(function($val){return intval($val);}, explode(',', $params['users']));
        $rowset = $this->_model->getCalendar($start, $end, $users, $this->user);
        #Zend_Debug::dump($rowset);
        $groups = new Core_Model_Groups();
        $groups = $groups->getArray();
        $rows = array();
        foreach($rowset as $row) {
            #Zend_Debug::dump($row->toArray());
            if(empty($rows[$row->date][$row->zone]['visit'])) $rows[$row->date][$row->zone]['visit'] = 0;
            if(empty($rows[$row->date][$row->zone]['all'])) $rows[$row->date][$row->zone]['all'] = 0;
            if(!is_null($row->time_visit)) $rows[$row->date][$row->zone]['visit'] += $row->count;
            $rows[$row->date][$row->zone]['all'] += $row->count;
            $rows[$row->date][$row->zone]['row'] = $row;
        }
        $json = array();
        foreach($rows as $date => $zones) {
            foreach($zones as $zone => $row) {
                $json[] = array(
                    'id' => $date,
                    'title' => '[' . $row['all'] . '/' . $row['visit'] . '] Маршрутный лист ' . $row['row']->name,
                    'start' => $date,
                    'end'   => $date,
                    'allDay' => true,
                    'url' => "javascript:Core.Route('edit','Маршрутный лист " . $row['row']->name . " на " . $date . "',{date:" . strtotime($date) . ",zone:" . $zone . "});",
                    'className' => ((strtotime($date) >= mktime(0,0,0,date('m'),date('d'),date('Y'))) ? $this->status[0] : $this->status[2])
                    #'color' => 'red'
                );
            }
        }
        echo $this->_helper->json($json, true);
    }

    public function addAction()
    {
        $request = $this->getRequest();
        $params = $request->getParams();
        $users = new Core_Model_Users();
        $this->view->users = $users->getList($this->user->id);
        $this->view->date = $params['start'];
    }

    public function editAction()
    {
        $request = $this->getRequest();
        $params = $request->getParams();
        $now = new Zend_Date($params['date']);
        $users = new Core_Model_Users();
        $this->view->date = $params['date'];
        $this->view->zone = $params['zone'];
        $this->view->routes = $this->_model->getRoute($params['date'], (int)$params['zone']);
        $this->view->users = $users->getList($this->user->id);
        if($now->getTimestamp() < mktime(0,0,0,date('m'),date('d')-1,date('Y'))) {
            $this->render('load');
            return;
        }
        #$this->view->date = $params['start'];
    }

    public function saveAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        #$this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $params = $request->getParams();
        #print_r($params);
        #return;
        try {
            $now = new Zend_Date($params['date']);
            $current = new Zend_Date();
            $this->_model->getAdapter()->beginTransaction();
            $notify = new Core_Model_Notify();

            # Получаем маршрутный лист
            $routes = $this->_model->getRoute($now->getTimestamp(), (int)$params['zone']);
            #print_r(count($routes));
            #return;
            $ids = array();
            $add = $edit = $delete = 0;
            $filters = array('*' => array('StripTags'));
            $validators = array(
                'client' => array(
                    //'alnum',
                    array('StringLength', 3, 150),
                    'presence' => 'required'
                ),
                'address' => array(
                    array('StringLength', 3, 150),
                    'presence' => 'required'
                )
            );
            $options = array(
                'escapeFilter' => 'StringTrim',
                'missingMessage' => "Не все обязательные поля заполнены!",
                'notEmptyMessage' => "Поле %field%' обязательное для заполнения!"
            );

            # Перебираем маршрутный лист по каждому пункту
            foreach($routes as $row) {
                array_push(&$ids, $row->id);
                # Если данный маршрут не найден в параметрах, значит он был удалён
                if(!in_array($row->id, $params['edit'])) {
                    if($row->user_id != $this->user->id) { throw new Zend_Db_Exception('К данным записям вам доступ запрещён!'); }
                    # Удаляем его в БД
                    $row->delete();
                    $delete++;
                } else {
                    #if($row->user_id != $this->user->id && empty($params['row'][$row->id]['time_visit'])) { throw new Zend_Db_Exception('К данным записям вам доступ запрещён! (E)'); }
                    $params['row'][$row->id]['client'] = stripslashes($params['row'][$row->id]['client']);
                    #$client_id = $clients->add($params['client'][$id]);
                    $params['row'][$row->id]['address'] = stripcslashes($params['row'][$row->id]['address']);
                    if(!empty($params['row'][$row->id]['time_visit'])) {
                        $params['row'][$row->id]['time_visit'] = $current->toString('HH:mm') . ':00';
                    }
                    if($row->user_id != $this->user->id) {
                        unset(
                            $params['row'][$row->id]['client'],
                            $params['row'][$row->id]['address'],
                            $params['row'][$row->id]['hp'],
                            $params['row'][$row->id]['posom'],
                            $params['row'][$row->id]['date']
                        );
                    }
                    #$address->add($client_id, $params['address'][$id]);
                    #$row->client = $params['client'][$id];
                    #$row->mpz = $params['mpz'][$id];
                    #$row->proch = $params['proch'][$id];
                    #$row->sku_mpz = $params['sku_mpz'][$id];
                    #$row->sku_proch = $params['sku_proch'][$id];
                    #$row->address = $params['address'][$id];
                    #$row->time_visit = $params['time_visit'][$id];
                    #$row->hp = $params['hp'][$id];
                    #$row->posom = $params['posom'][$id];
                    #$row->date = $now->toString('YYYY-MM-dd');
                    #$row->time_visit = $now->toString('HH:mm');
                    #$row->result = $params['result'][$id];
                    #$row->comment = $params['comment'][$id];

                    # Если найден, редактируем
                    $row->setFromArray($params['row'][$row->id]);
                    $row->save();
                    $edit++;
                }
            }
            # Теперь создаём новые
            $users = new Core_Model_Users();
            $city = $users->getCity((int)$params['zone']);
            $clients = new Core_Model_Clients();
            $address = new Core_Model_Address();
            $exists = array();
            foreach($params['new']['client'] as $id => $value) {
                /*$input = new Zend_Filter_Input($filters, $validators, $params['new'], $options);
                if(!$input->isValid()) {
                    $error = '';
                    foreach($input->getMessages() as $messageId => $messageArr) {
                        foreach($messageArr as $id => $message) {
                            $error .= '<div class="message error"><span class="strong">ОШИБКА!</span> ' . $message . '</div>';
                        }
                    }
                    echo $this->_helper->json(array('success' => false, 'result' => $error), true);
                }*/
                if(empty($params['new']['client'][$id]) || empty($params['new']['address'][$id])) continue;
                $params['new']['client'][$id] = stripslashes($params['new']['client'][$id]);
                $client_id = $clients->add($params['new']['client'][$id]);
                $params['new']['address'][$id] = stripcslashes($params['new']['address'][$id]);
                $address->add($client_id, $params['new']['address'][$id]);
                $row = $this->_model->createRow();
                $row->client = $params['new']['client'][$id];
                $row->address = $params['new']['address'][$id];
                $row->mpz = $params['new']['mpz'][$id];
                $row->proch = $params['new']['proch'][$id];
                $row->sku_mpz = $params['new']['sku_mpz'][$id];
                $row->sku_proch = $params['new']['sku_proch'][$id];
                $row->hp = $params['new']['hp'][$id];
                $row->posom = $params['new']['posom'][$id];
                $row->result = $params['new']['result'][$id];
                $row->comment = $params['new']['comment'][$id];
                $row->date = $now->toString('YYYY-MM-dd');
                $row->user_id = $this->user->id; # ????
                $row->city = $city; # ????
                $row->zone = (int)$params['zone']; # ????
                $row->save();
                $add++;
            }
            $this->_model->getAdapter()->commit();
            # Если было редактирование существуещего маршрутного листа
            $p = '{date:' . $now->getTimestamp() . ',zone:' . (int)$params['zone'] . '}';
            if(count($routes) > 0) {
                $desc = ' Изменено: ' . $edit . ', Добавлено: ' . $add . ', Удалено: ' . $delete;
                $notify->add(
                    (int)$params['zone'],
                    'У вас изменения в маршрутном листе на <b>' . $now->toString('YYYY-MM-dd') . '</b> (<a href="javascript:Core.Route(\'load\',\'Маршрутный лист ' . $now->toString('YYYY-MM-dd') . '\',' . $p . ');">посмотреть</a>)'
                );
            } else {
                $desc = ' Добавлено: ' . $add;
                $notify->add(
                    (int)$params['zone'],
                    'Вам доступен новый маршрутный лист на <b>' . $now->toString('YYYY-MM-dd') . '</b> (<a href="javascript:Core.Route(\'load\',\'Маршрутный лист ' . $now->toString('YYYY-MM-dd') . '\',' . $p . ');">посмотреть</a>)'
                );
            }
            echo $this->_helper->json(array('success' => true, 'result' => '<div class="message success"><span class="strong">УСПЕШНО!</span> Маршрутный лист сохранён.' . $desc . '</div>'), true);
        } catch (Zend_Db_Exception $e) {
            $this->_model->getAdapter()->rollBack();
            echo $this->_helper->json(array('success' => false, 'result' => '<div class="message error"><span class="strong">ОШИБКА!</span> ' . $e->getMessage() . '<br /></div>'), true);
        } catch (Zend_Exception $e) {
            echo $this->_helper->json(array('success' => false, 'result' => '<div class="message error"><span class="strong">ОШИБКА!</span> ' . $e->getMessage() . '</div>'), true);
        }
    }

    public function loadAction()
    {
        try {
        $request = $this->getRequest();
        $params = $request->getParams();
        $this->view->date = strtotime($params['date']);
        $this->view->zone = $params['zone'];
        $this->view->routes = $this->_model->getRoute($params['date'], (int)$params['zone']);
        } catch (Zend_Exception $e) {
            echo $e->getMessage();
        }
    }

    public function completedAction()
    {

        $this->_helper->viewRenderer->setNoRender();
        #$this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $params = $request->getParams();

        if($request->isPost()) {
            try {
                $filters = array('*' => array('StripTags'));
                $validators = array(
                    'result' => array(
                        //'alnum',
                        array('StringLength', 3, 150)
                        //'presence' => 'required'
                    ),
                    'comment' => array(
                        array('StringLength', 3, 150)
                        //'presence' => 'required'
                    )
                );
                $options = array(
                    'escapeFilter' => 'StringTrim',
                    'missingMessage' => "Не все обязательные поля заполнены!",
                    'notEmptyMessage' => "Поле %field%' обязательное для заполнения!"
                );
                $input = new Zend_Filter_Input($filters, $validators, $params, $options);
                if(!$input->isValid()) {
                    $error = '';
                    foreach($input->getMessages() as $messageId => $messageArr) {
                        foreach($messageArr as $id => $message) {
                            $error .= '<div class="message error"><span class="strong">ОШИБКА!</span> ' . $message . '</div>';
                        }
                    }
                    echo $this->_helper->json(array('success' => false, 'result' => $error), true);
                }
                $now = new Zend_Date();
                $this->_model->getAdapter()->beginTransaction();
                $notify = new Core_Model_Notify();
                foreach($params['result'] as $id => $name) {
                    if(empty($params['result'])) continue;
                    $row = $this->_model->find((int)$id)->current();
                    $row->time_visit = $now->toString('HH:mm');
                    $row->result = $params['result'][$id];
                    $row->comment = $params['comment'][$id];
                    $row->save();
                }
                $p = '{date:' . $row->date . ',zone:' . $row->zone . '}';
                $notify->add(
                    $row->user_id,
                    'У вас изменения в маршрутном листе на <b>' . $row->date . '</b> (<a href="javascript:Core.Route(\'load\',\'Маршрутный лист ' . $row->date . '\',' . $p . ');">посмотреть</a>)');
                $this->_model->getAdapter()->commit();
                echo $this->_helper->json(array('success' => true, 'result' => '<div class="message success"><span class="strong">УСПЕШНО!</span> Маршрутный лист сохранён</div>'), true);
            } catch (Zend_Db_Exception $e) {
                $this->_model->getAdapter()->rollBack();
                echo $this->_helper->json(array('success' => false, 'result' => '<div class="message error"><span class="strong">ОШИБКА!</span> ' . $e->getMessage() . '<br /></div>'), true);
            } catch (Zend_Exception $e) {
                echo $this->_helper->json(array('success' => false, 'result' => '<div class="message error"><span class="strong">ОШИБКА!</span> ' . $e->getMessage() . '</div>'), true);
            }
        }
    }

}


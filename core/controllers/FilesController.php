<?php

class FilesController extends Zend_Controller_Action
{

    protected $_session;

    protected $_model;

    protected $_acl;

    public function init() {
		$this->_helper->layout->disableLayout();
        $this->_model = new Core_Model_Files();
        $this->_acl = Zend_Auth::getInstance();
    }

    public function indexAction() {
        $request = $this->getRequest();
        $params = $request->getParams();

        if($this->_request->isPost()) {
            #Zend_Debug::dump($_FILES); exit();
            $mime = $_FILES['file']['type'];
            /* Uploading Document File on Server */
            $upload = new Zend_File_Transfer_Adapter_Http();
            $ext = array_pop(explode(".", $upload->getFileName('file')));
            $hash = (int)$this->_acl->getIdentity()->id . '_' . $upload->getHash('md5','file') . '.' . $ext;
            #echo "HASH: " . $hash . "<br />\n";
            #$upload->addFilter('LowerCase')
            $upload->addFilter('Rename', array('target' => APPLICATION_PATH . '/../htdocs/storage/' . $hash, 'overwrite' => false))
            ->addValidator('NotExists', false, APPLICATION_PATH . '/../htdocs/storage/' . $hash)
            ->addValidator('Extension', false, array('xls', 'xlsx' , 'pdf' , 'txt' , 'doc' , 'docx', 'odt', 'odx', 'jpg', 'jpeg', 'tiff', 'gif', 'bmp', 'png', 'zip', 'rar', '7zip', 'tar', 'gz', 'bz2'))
            ->addValidator('FilesSize', false, array('min' => '1kB', 'max' => '20MB'))
            ->addValidator('Count', false , 1);
            try {
                // upload received file(s)
                $res = $upload->receive();
                #Zend_Debug::dump($res);
                #$info = $upload->getFileInfo('file');
                $this->_model->getAdapter()->beginTransaction();
                $id = $this->_model->add(
                #Zend_Debug::dump(array(
                    (int)$this->_acl->getIdentity()->id,
                    $this->_request->getParam('title','Без имени'),
                    $upload->getFileSize('file'),
                    $mime,
                    $hash
                );

                $filesusers = new Core_Model_Filesusers();
                if(count($params['users']) == 0) throw new Zend_Exception('Вы не выбрали пользователей!');
                foreach($params['users'] as $user_id) { if((int)$user_id==0) throw new Zend_Exception('Выбраный вами пользовать несуществует'); }
                foreach($params['users'] as $user_id) {
                    $row = $filesusers->createRow();
                    $row->file_id = $id;
                    $row->user_id = (int)$user_id;
                    $row->status = 0;
                    $row->save();
                    #$notify->add((int)$user_id, 'Дела - Вам назначено задание от <b>' . $this->user->name . '</b>, <b>' . $input->title . '</b> (<a href="javascript:Core.Task(\'load\',\'Задача #' . $id . '\',{id:' . $id . '});">посмотреть</a>)');
                }


                $this->_model->getAdapter()->commit();



                #exit();
                #Zend_Debug::dump($upload->getFileInfo('file'));
                /*

Filter & Valid:
bool(false)
bool(true)
array(0) {
}
Name of uploaded file: /tmp/back card.jpg
File Size: 1.38MB
File's Mime Type: application/octet-stream
array(1) {
  ["file"] => array(12) {
    ["name"] => string(13) "back card.jpg"
    ["type"] => string(24) "application/octet-stream"
    ["tmp_name"] => string(14) "/tmp/php9m0Xfa"
    ["error"] => int(0)
    ["size"] => string(7) "1451975"
    ["options"] => array(4) {
      ["ignoreNoFile"] => bool(false)
      ["useByteString"] => bool(true)
      ["magicFile"] => NULL
      ["detectInfos"] => bool(true)
    }
    ["validated"] => bool(true)
    ["received"] => bool(false)
    ["filtered"] => bool(false)
    ["validators"] => array(5) {
      [0] => string(25) "Zend_Validate_File_Upload"
      [1] => string(28) "Zend_Validate_File_NotExists"
      [2] => string(28) "Zend_Validate_File_Extension"
      [3] => string(28) "Zend_Validate_File_FilesSize"
      [4] => string(24) "Zend_Validate_File_Count"
    }
    ["destination"] => string(4) "/tmp"
    ["filters"] => array(2) {
      [0] => string(26) "Zend_Filter_File_LowerCase"
      [1] => string(23) "Zend_Filter_File_Rename"
    }
  }
}
                 */
                #$this->_helper->flashMessenger->setNamespace('error')->addMessage('<div class="message success"><span class="strong">УСПЕШНО!</span> Новый файл добавлен.</div>');
                $this->_helper->flashMessenger->setNamespace('error')->addMessage('Новый файл добавлен');
            } catch (Zend_Db_Exception $e) {
                $this->getAdapter()->rollBack();
                @unlink(APPLICATION_PATH . '/../htdocs/storage/' . $hash);
                $this->_helper->flashMessenger->setNamespace('error')->addMessage('ОШИБКА БД: ' . addslashes($e->getMessage()));
            } catch (Zend_Exception $e) {
                @unlink(APPLICATION_PATH . '/../htdocs/storage/' . $hash);
                #$this->_helper->flashMessenger->setNamespace('error')->addMessage('<div class="message error"><span class="strong">ОШИБКА!</span> ' . $e->getMessage() . '</div>');
                $this->_helper->flashMessenger->setNamespace('error')->addMessage('ОШИБКА: ' . addslashes($e->getMessage()));
            }
            $this->_helper->redirector->gotoUrl('/#files');
        } else {
            $users = new Core_Model_Users();
            $this->view->users = $users->getList((int)$this->_acl->getIdentity()->id);
            $this->view->items = $this->_model->getAll((int)$this->_acl->getIdentity()->id);
        }
    }

    public function getAction() {
        try {
            $this->_helper->viewRenderer->setNoRender();

            $file_id = $this->_request->getParam('id');

            if($file = $this->_model->getFile($file_id)) {
                if($file->user_id != (int)$this->_acl->getIdentity()->id) {
                    $users = new Core_Model_Filesusers();
                    if($row = $users->fetchRow('file_id = ' . (int)$file->id . ' AND user_id = ' . (int)$this->_acl->getIdentity()->id)) {
                        $row->status = 1;
                        $row->save();
                    } else {
                        echo "Нет прав доступа...";
                    }
                }
                #Zend_Debug::dump($file);
                $fileFullName = APPLICATION_PATH . '/../htdocs/storage/' . $file->filename;
                $ftime = date("D, d M Y H:i:s T", filemtime($fileFullName));
                $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->setHeader('Pragma', 'public', true)
                    ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
                    #->setHeader('Last-Modified', $ftime)
                    #->setHeader('Accept-Ranges', 'bytes')
                    ->setHeader('Content-type',  $file->mimetype, true)
                    #->setHeader('Content-Length', filesize($fileFullName))
                    #->setHeader('Content-Disposition', 'attachment; filename=result.pdf')
                    ->setHeader('Content-Disposition', 'attachment; filename="' . $file->title . "." . array_pop(explode(".", $file->filename)) . '"');
                    #->clearBody();
                $this->getResponse()->sendHeaders();
                readfile($fileFullName);
            } else {
                echo "Файл не найден...";
            }
        } catch (Exception $e) {
            die ('Application error: ' . $e->getMessage() . ' on file ' . $e->getFile() . ' on line ' . $e->getLine());
        }
    }


    public function deleteAction() {
        try {
            $this->_helper->viewRenderer->setNoRender();
            $file_id = $this->_request->getParam('id');
            if($file = $this->_model->getFile($file_id)) {
                $fileFullName = APPLICATION_PATH . '/../htdocs/storage/' . $file->filename;
                if(unlink($fileFullName)) {
                    $this->_model->remove($file->id);
                    echo  "ok";
                } else {
                    echo "Файл не удалён из хранилища";
                }
                #$this->_helper->redirector->gotoUrl('/#files');
            } else {
                echo "Файл не найден...";
            }
        } catch (Exception $e) {
            die ('Application error: ' . $e->getMessage() . ' on file ' . $e->getFile() . ' on line ' . $e->getLine());
        }
    }

}


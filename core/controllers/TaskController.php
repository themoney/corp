<?php

class TaskController extends Zend_Controller_Action
{

    protected $_model;

    protected $status = array(0 => 'new', 1 => 'read', 2 => 'completed');

    protected $mark = array(0 => '☐', 1 => '☐', 2 => '☑');

    protected $_acl;

    protected $user;

    public function init()
    {
        /* Initialize action controller here */
        $this->_model = new Core_Model_Tasks();
        $this->_acl = Zend_Auth::getInstance();
        #$this->_model->setUser((int)$this->_acl->getIdentity()->id);
        $this->user = $this->_acl->getIdentity();
        $this->_helper->layout->disableLayout();
        #$this->_helper->AjaxContext()->addActionContext('index', 'json')->initContext('json');
    }

    public function indexAction()
    {
        $request = $this->getRequest();
        $params = $request->getParams();
        $start = $params['start'];#time()-(60*60*24);
        $end = $params['end'];#time()+(60*60*24);
        $users = array_map(function($val){return intval($val);}, explode(',', $params['users']));
        $rowset = $this->_model->getCalendar($start, $end, $users);
        #Zend_Debug::dump($rowset);
        $json = array();

        $json = array();
        foreach($rowset as $row) {
            $status = $row->status;
            $json[] = array(
                'id' => $row->id,
                'title' => $this->mark[$status] . ' ' . $row->title . ((($params['end']-$params['start'])==86400) ? "\n" . $row->text : ''),
                'start' => $row->start,
                'end'   => $row->end,
                'allDay' => false,
                'url' => "javascript:Core.Task('load','Задача #" . $row->id . "',{id:" . $row->id . "});",
                'className' => $this->status[$status],
                #'color' => 'red'
            );
        }
        echo $this->_helper->json($json, true);
    }

    public function addAction()
    {
        $request = $this->getRequest();
        $params = $request->getParams();
        $users = new Core_Model_Users();
        $this->view->users = $users->getList($this->user->id);
        $this->view->date = $params['start'];
    }

    public function saveAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        #$this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $params = $request->getParams();
        if($request->isPost()) {
            try {
                $filters = array('*' => array('StripTags'));
                $validators = array(
                    'title' => array(
                        //'alnum',
                        array('StringLength', 1, 150),
                        'presence' => 'required'
                    ),
                    'text' => array(
                        array('StringLength', 1, 1500),
                        'presence' => 'required'
                    )
                );
                $options = array(
                    'escapeFilter' => 'StringTrim',
                    'missingMessage' => "Не все обязательные поля заполнены!",
                    'notEmptyMessage' => "Поле %field%' обязательное для заполнения!"
                );
                $input = new Zend_Filter_Input($filters, $validators, $params, $options);
                if(!$input->isValid()) {
                    $error = '';
                    foreach($input->getMessages() as $messageId => $messageArr) {
                        foreach($messageArr as $id => $message) {
                            $error .= '<div class="message error"><span class="strong">ОШИБКА!</span> ' . $message . '</div>';
                        }
                    }
                    echo $this->_helper->json(array('success' => false, 'result' => $error), true);
                }
                $now = new Zend_Date();
                $start = new Zend_Date($params['start']['date'] . " " . $params['start']['time'], 'dd.MM.YYYY HH:mm');
                $end = new Zend_Date($params['end']['date'] . " " . $params['end']['time'], 'dd.MM.YYYY HH:mm');
                if($start->getTimestamp() > $end->getTimestamp()) throw new Zend_Exception('Дата начала не может быть позднее даты окончания');
                $completed = 0;
                if(empty($params['completed'])) {
                    if($start->getTimestamp() < $now->getTimestamp()) throw new Zend_Exception('Дата начала не может быть раньше текущего времени');
                    if($end->getTimestamp() < $now->getTimestamp()) throw new Zend_Exception('Дата окончания не может быть раньше текущего времени');
                } else {
                    $completed = 2;
                }
                $this->_model->getAdapter()->beginTransaction();
                $row = $this->_model->createRow();
                $row->user_id = $this->user->id;
                $row->title = $input->title;
                $row->text = $input->text;
                $row->start = $start->toString('YYYY-MM-dd HH:mm:ss');
                $row->end = $end->toString('YYYY-MM-dd HH:mm:ss');
                $row->status = $completed;
                $id = $row->save();
                $notify = new Core_Model_Notify();
                $tasksusers = new Core_Model_Tasksusers();
                if((bool)$params['forme']) {
                    $row = $tasksusers->createRow();
                    $row->task_id = $id;
                    $row->user_id = $this->user->id;
                    $row->status = $completed;
                    $row->save();
                    $notify->add($this->user->id, 'Дела - Вы назначили личное задание, <b>' . $input->title . '</b> (<a href="javascript:Core.Task(\'load\',\'Задача #' . $id . '\',{id:' . $id . '});">посмотреть</a>)');
                } else {
                    if(count($params['users']) == 0) throw new Zend_Exception('Вы не выбрали пользователей!');
                    foreach($params['users'] as $user_id) { if((int)$user_id==0) throw new Zend_Exception('Выбраный вами пользовать несуществует'); }
                    foreach($params['users'] as $user_id) {
                        $row = $tasksusers->createRow();
                        $row->task_id = $id;
                        $row->user_id = (int)$user_id;
                        $row->status = $completed;
                        $row->save();
                        $notify->add((int)$user_id, 'Дела - Вам назначено задание от <b>' . $this->user->name . '</b>, <b>' . $input->title . '</b> (<a href="javascript:Core.Task(\'load\',\'Задача #' . $id . '\',{id:' . $id . '});">посмотреть</a>)');
                    }
                }
                $this->_model->getAdapter()->commit();
                echo $this->_helper->json(array('success' => true, 'result' => '<div class="message success"><span class="strong">УСПЕШНО!</span> Задача создана</div>'), true);
            } catch (Zend_Db_Exception $e) {
                $this->_model->getAdapter()->rollBack();
                echo $this->_helper->json(array('success' => false, 'result' => '<div class="message error"><span class="strong">ОШИБКА!</span> ' . $e->getMessage() . '</div>'), true);
            } catch (Zend_Exception $e) {
                echo $this->_helper->json(array('success' => false, 'result' => '<div class="message error"><span class="strong">ОШИБКА!</span> ' . $e->getMessage() . '</div>'), true);
            }
        }
    }

    public function addmonitoringAction() {
        $request = $this->getRequest();
        $params = $request->getParams();

        if($this->_request->isPost()) {
            try {
                $title = "Внимание Мониторинг!";
                $text = "Сегодня проводится мониторинг цен, бланк мониторинга в разделе файлы.";
                $date = new Zend_Date($params['date']);
                $this->_model->getAdapter()->beginTransaction();
                $row = $this->_model->createRow();
                $row->user_id = $this->user->id;
                $row->title = $title;
                $row->text = $text;
                $row->start = $date->toString('YYYY-MM-dd 09:00:00');
                $row->end = $date->toString('YYYY-MM-dd 18:00:00');
                $row->status = 0;
                $id = $row->save();
                $notify = new Core_Model_Notify();
                $tasksusers = new Core_Model_Tasksusers();
                $users = new Core_Model_Users();
                $users = $users->getList($this->user->id);
                $city = array();
                foreach($users as $user) {
                    $city[] = $user->city;
                    $row = $tasksusers->createRow();
                    $row->task_id = $id;
                    $row->user_id = (int)$user->id;
                    $row->status = 0;
                    $row->save();
                    $notify->add((int)$user->id, 'Дела - Вам назначено задание от <b>' . $this->user->name . '</b>, <b>' . $title . '</b> (<a href="javascript:Core.Task(\'load\',\'Задача #' . $id . '\',{id:' . $id . '});">посмотреть</a>)');
                }
                $this->_model->getAdapter()->commit();
                $this->_helper->flashMessenger->setNamespace('error')->addMessage('Проведение мониторинга цен назначено на ' . $date->toString('dd.MM.YYYY') . ' для ' . implode(', ', array_unique($city)));
            } catch (Zend_Db_Exception $e) {
                $this->_model->getAdapter()->rollBack();
                $this->_helper->flashMessenger->setNamespace('error')->addMessage('ОШИБКА БД: ' . addslashes($e->getMessage()));
            } catch (Zend_Exception $e) {
                $this->_helper->flashMessenger->setNamespace('error')->addMessage('ОШИБКА: ' . addslashes($e->getMessage()));
            }
            $this->_helper->redirector->gotoUrl('/#monitoring');
        } else {
            //if(in_array($this->_acl->getIdentity()->group_id, array(1,2)))$this->view->form = new Core_Form_Monitoring();
        }
    }

    public function loadAction()
    {
        $request = $this->getRequest();
        $params = $request->getParams();
        $this->view->task = $this->_model->find((int)$params['id'])->current();
        $groups = new Core_Model_Groups();
        $this->view->groups = $groups->getArray();
        $tasks = new Core_Model_Tasksusers();
        $this->view->tasks = $tasks->getStatus((int)$this->view->task->id);
        $this->view->users = $this->view->task->findManyToManyRowset('Core_Model_Users', 'Core_Model_Tasksusers');

        $users = new Core_Model_Tasksusers();
        $users->update(array('status' => 1), array('task_id = ' . (int)$this->view->task->id, 'user_id = ' . $this->user->id));

        if((int)$this->view->task->status == 0) {
            $this->view->task->status = 1;
            $this->view->task->save();
        }
    }

    public function completedAction()
    {
        $request = $this->getRequest();
        $params = $request->getParams();
        $task = $this->_model->find((int)$params['id'])->current();
        $users = new Core_Model_Tasksusers();
        $users->update(array('status' => 2), array('task_id = ' . (int)$task->id, 'user_id = ' . $this->user->id));
        $tasks = $users->fetchAll('task_id = ' . (int)$task->id);
        if(count($tasks) > 0) {
            $complete = 0;
            foreach($tasks as $row) {
                if((int)$row->status == 2) $complete++;
            }
            $notify = new Core_Model_Notify();
            $notify->add($task->user_id, 'Задача <b>' . $task->title . '</b> выполнена пользователем <b>' . $this->user->name . '</b> (<a href="javascript:Core.Task(\'load\',\'Задача #' . $task->title . '\',{id:' . $task->id . '});">посмотреть</a>)');
            if($complete > 0 && count($tasks) == $complete) {
                $task->status = 2;
                $task->save();
                $notify = new Core_Model_Notify();
                $notify->add($task->user_id, 'Задача <b>' . $task->title . '</b> выполнена всеми пользователями (<a href="javascript:Core.Task(\'load\',\'Задача #' . $task->title . '\',{id:' . $task->id . '});">посмотреть</a>)');
            }
        }
    }

}


<?php

class NotifyController extends Zend_Controller_Action
{

    protected $_session;

    protected $_model;

    protected $_acl;

    public function init() {
		$this->_helper->layout->disableLayout();
        #$this->_session = new Zend_Session_Namespace('Form_Stats');
        $this->_model = new Core_Model_Notify();
        $this->_acl = Zend_Auth::getInstance();
    }

    public function indexAction() {
        $request = $this->getRequest();
        $params = $request->getParams();
        $this->_model->update(array('read' => '1'), array('user_id = ' . (int)$this->_acl->getIdentity()->id));
        $this->view->items = $this->_model->getAll((int)$this->_acl->getIdentity()->id);
    }

    public function pollAction() {
        $json = $this->_model->getUnread((int)$this->_acl->getIdentity()->id)->toArray();
        $this->_helper->json($json, true);
    }

    public function onlineAction() {
        $chat = new Core_Model_Chat();

        $users = new Core_Model_Users();
        $json = $users->getOnline((int)$this->_acl->getIdentity()->id)->toArray();
        $this->_helper->json($json, true);
    }

}


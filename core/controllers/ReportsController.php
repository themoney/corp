<?php

class ReportsController extends Zend_Controller_Action
{

    protected $_session;

    protected $_model;

    protected $_acl;

    public function init() {
		$this->_helper->layout->disableLayout();
        $this->_model = new Core_Model_Reports();
        $this->_acl = Zend_Auth::getInstance();
    }

    public function indexAction() {
        /*$request = $this->getRequest();
        $params = $request->getParams();

        if($this->_request->isPost()) {
            try {
                $this->_helper->flashMessenger->setNamespace('error')->addMessage('Новый файл добавлен');
            } catch (Zend_Db_Exception $e) {
                $this->_helper->flashMessenger->setNamespace('error')->addMessage('ОШИБКА БД: ' . addslashes($e->getMessage()));
            } catch (Zend_Exception $e) {
                $this->_helper->flashMessenger->setNamespace('error')->addMessage('ОШИБКА: ' . addslashes($e->getMessage()));
            }
            $this->_helper->redirector->gotoUrl('/#reports');
        } else {
            $users = new Core_Model_Users();
            $this->view->users = $users->getList((int)$this->_acl->getIdentity()->id);
            $this->view->items = $this->_model->getAll((int)$this->_acl->getIdentity()->id);
        }*/
        $this->view->formMonitoring = $this->view->formFuel = '';
        if(in_array($this->_acl->getIdentity()->group_id, array(1,2))) $this->view->formMonitoring = new Core_Form_Monitoring();
        if($this->_acl->getIdentity()->group_id == 1) $this->view->formFuel = new Core_Form_Fuel();
        $this->view->formReports = new Core_Form_Reports();
    }

    public function pdfAction() {
        try {
            $this->_helper->viewRenderer->setNoRender();
            $pdf = new Core_Model_Pdf();
            $pdf->cellWidths = array(
                30, // Клиент 250px
                280, // Адрес 375px
                655, // Кол-во посещений 45px
                700, // Мпз 40px
                740, // Прочка 40px
                790, // Зона 40px
                820
            );
            $pdf->tableHeader = array('Клиент', 'Адрес', 'Кол-во', 'Мпз', 'Прочка', 'Зона');
            #$pdf->getTotalsFrom = 8;
            #$model = new Core_Model_Routes();
            $params = $this->_request->getParams();
            #Zend_Debug::dump($params);
            $from = ((empty($params['from'])) ? false : $params['from']);
            $to = ((empty($params['to'])) ? false : $params['to']);
            $users = new Core_Model_Users();
            $zone = $users->getZones();
            $rows = $this->_model->getReport($from, $to, $params['type'], $params['city'], $params['client'], $params['zone']);
            #$array = $rows->toArray();
            $items = array();
            foreach($rows as $row) {
                $items[] = array(substr($row->client, 0, 150), substr($row->address, 0, 150), $row->count, round($row->mpz) . "%", round($row->prochka) . "%", $zone[$row->zone]);
            }
            if(!empty($params['zone'])) $params['zone'] = $zone[(int)$params['zone']];
            $pdf->renderAndSend($items, $params, 'reports.pdf', $this->getResponse());
        } catch (Exception $e) {
            die ('Application error: ' . $e->getMessage() . ' on file ' . $e->getFile() . ' on line ' . $e->getLine());
        }

    }

    public function htmlAction() {
        try {
            #$this->_helper->viewRenderer->setNoRender();
            $params = $this->_request->getParams();
            $from = ((empty($params['from'])) ? false : $params['from']);
            $to = ((empty($params['to'])) ? false : $params['to']);
            $users = new Core_Model_Users();
            $zone = $users->getZones();
            $items = $this->_model->getReport($from, $to, $params['type'], $params['city'], $params['client'], $params['zone']);
            foreach($items as $row) {
                $row->zone = $zone[$row->zone];
                $row->mpz = round($row->mpz);
                $row->prochka = round($row->prochka);
            }
            #Zend_Debug::dump($items);
            #exit;
            $this->view->items = $items;
        } catch (Exception $e) {
            die ('Application error: ' . $e->getMessage() . ' on file ' . $e->getFile() . ' on line ' . $e->getLine());
        }

    }

    public function fuelAction() {
        try {
            #$this->_helper->viewRenderer->setNoRender();
            $params = $this->_request->getParams();
            $this->view->start = $from = ((empty($params['from'])) ? false : $params['from']);
            $this->view->end = $to = ((empty($params['to'])) ? false : $params['to']);
            $users = new Core_Model_Users();
            $zone = $users->getZones();
            $fuel = new Core_Model_Fuel();
            $items = $fuel->getReport($from, $to, $params['zone']);
            foreach($items as &$row) {
                $row['zone'] = $zone[$row['user_id']];
                $row['day'] = round($row['day']);
                $row['month'] = round($row['month']);
                #Zend_Debug::dump($row);
            }
            $this->view->items = $items;
        } catch (Exception $e) {
            die ('Application error: ' . $e->getMessage() . ' on file ' . $e->getFile() . ' on line ' . $e->getLine());
        }
    }

    public function graphAction() {
        $request = $this->getRequest();
        $params = $request->getParams();

        try {
            $this->_helper->viewRenderer->setNoRender();
            require_once('pChart2.1.1/class/pData.class.php');
            require_once('pChart2.1.1/class/pDraw.class.php');
            require_once('pChart2.1.1/class/pImage.class.php');
            $monitoring = new Core_Model_Monitoring();
            $data = $monitoring->getAllData($params['from'], $params['to'], $params['product'], $params['supplier']);
            $series=$serie_names=array();
            foreach($data as $row) {
                $series[$row->supplier_id][$row->date] = round($row->avg. 2);
                $serie_names[$row->date] = $row->date;
            }
            $graph_data = new pData();
            $suppliersModel = new Core_Model_Suppliers();
            $rows = $suppliersModel->fetchAll();
            $suppliers = array();
            $name = '';
            foreach($rows as $row) {
                if($row->id < 17) continue;
                $suppliers[$row->id] = $row->name;
                $graph_data->addPoints($series[$row->id], $row->name);
                if($row->id == 17) { $name = $row->name; }
            }
            $graph_data->addPoints($serie_names,"X");
            #$graph_data->setSerieDescription("X","");
            $graph_data->setSerieWeight($name, 2);
            $graph_data->setAbscissa("X");
            $graph_data->loadPalette(APPLICATION_PATH . "/../library/pChart2.1.1/palettes/light.color");
            $width = 1024; $height = 250;
            $chart = new pImage($width,$height,$graph_data);
            $chart->setFontProperties(array("FontName"=> APPLICATION_PATH . "/../library/pChart2.1.1/fonts/verdana.ttf","FontSize"=>7));
            $chart->setGraphArea(20,10,($width-200),($height-15));
            $chart->drawScale(array("DrawXLines"=>true,"DrawSubTicks"=>true,"Mode"=>SCALE_MODE_START0,"GridR"=>0,"GridG"=>0,"GridB"=>0,"GridAlpha"=>20));
            foreach($suppliers as $id => $name) { if($id < 17) continue; $graph_data->setSerieDrawable($name, ($id !== 17)); }
            $chart->drawBarChart(array("DisplayValues"=>true,"DisplayColor"=>DISPLAY_AUTO,"Rounded"=>false,"Surrounding"=>60));
            #$chart->drawLineChart();
            foreach($suppliers as $id => $name) $graph_data->setSerieDrawable($name, ($id == 17));
            $chart->setShadow(true,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));
            $chart->drawLineChart(array("DisplayValues"=>true,"DisplayR"=>255,"DisplayG"=>0,"DisplayB"=>0,"Rounded"=>true));
            $chart->setShadow(false);
            foreach($suppliers as $id => $name) { if($id < 17) continue; $graph_data->setSerieDrawable($name, ($id !== 17)); }
            $chart->drawLegend(($width-190),20,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_VERTICAL,"BoxSize"=>7));

            /* Draw the chart */
            #$chart->drawSplineChart();
            #$chart->drawPlotChart();
            /* Write the data bounds */
            #$chart->writeBounds();

            $chart->Stroke();
        } catch (Exception $e) {
            exit('Application error: ' . $e->getMessage() . ' on file ' . $e->getFile() . ' on line ' . $e->getLine());
        }
    }
}


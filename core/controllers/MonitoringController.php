<?php

class MonitoringController extends Zend_Controller_Action
{

    protected $_session;

    protected $_model;

    protected $_acl;

    public function init() {
		$this->_helper->layout->disableLayout();
        $this->_model = new Core_Model_Monitoring();
        $this->_acl = Zend_Auth::getInstance();
    }

    public function indexAction() {
        $request = $this->getRequest();
        $params = $request->getParams();

        if($this->_request->isPost()) {
            try {
                $date = $params['date'];
                $data = new Core_Model_Excel($_FILES['file']['tmp_name'], true, "UTF-8");
                $excel = $data->dumptoarray();
                # Спецификация XLS файла мониторинга
                # Номер строки - описание
                # 1х2 - город
                # 2+3 - продукция
                # 4-29 - x1 поставщики, x2-x29 данные
                #
                # Работаем с таблицей
                #
                $this->_model->getAdapter()->beginTransaction();
                /*$ProductsModel = new Core_Model_Products();
                $products = array();
                $last = null;
                for($i=2;$i<25;$i++) {
                    if(!empty($excel[2][$i])) $last = $excel[2][$i];
                    $products[$i] = $last . (!empty($excel[3][$i]) ? ' ' . $excel[3][$i] : '');
                    $ProductsModel->add($i, $products[$i]);
                }*/
                $SuppliersModel = new Core_Model_Suppliers();
                $suppliers = array();
                $count = 0;
                for($x=4; $x < 21; $x++) {
                    /*if(!empty($excel[$x][1])) {
                        $suppliers[$x] = rtrim($excel[$x][1]);
                        $SuppliersModel->add($x, $suppliers[$x]);
                    }*/
                    for($i=2; $i < 25; $i++) {
                        if((float)$excel[$x][$i] > 0) {
                            $count++;
                            $this->_model->add($x, $i, (float)$excel[$x][$i], $date);
                        }
                    }
                }
                $this->_model->getAdapter()->commit();
                $this->_helper->flashMessenger->setNamespace('error')->addMessage('Лист мониторинга обработан, добавлено ' . $count . ' пунктов от ' . $date);
            } catch (Zend_Db_Exception $e) {
                $this->_model->getAdapter()->rollBack();
                $this->_helper->flashMessenger->setNamespace('error')->addMessage('ОШИБКА БД: ' . addslashes($e->getMessage()));
            } catch (Zend_Exception $e) {
                $this->_helper->flashMessenger->setNamespace('error')->addMessage('ОШИБКА: ' . addslashes($e->getMessage()));
            }
            $this->_helper->redirector->gotoUrl('/#monitoring');
        } else {
            //if(in_array($this->_acl->getIdentity()->group_id, array(1,2)))$this->view->form = new Core_Form_Monitoring();
        }
    }

}


<?php

class FuelController extends Zend_Controller_Action
{

    protected $_model;

    protected $status = array(0 => 'new', 1 => 'read', 2 => 'completed');

    protected $mark = array(0 => '☐', 1 => '☐', 2 => '☑');

    protected $_acl;

    protected $user;

    public function init()
    {
        /* Initialize action controller here */
        $this->_model = new Core_Model_Fuel();
        $this->_acl = Zend_Auth::getInstance();
        $this->user = $this->_acl->getIdentity();
        $this->_helper->layout->disableLayout();
    }

    public function indexAction()
    {
        $request = $this->getRequest();
        $params = $request->getParams();
        $json = array();
        $id = $this->_model->getCalendar($this->user);
        #Zend_Debug::dump($id);
        if($id) {
            $date = new Zend_Date();
            $json[] = array(
                'id' => md5(time().$this->user->id),
                'title' => 'Отчёт по ГСМ',
                'start' => $date->getTimestamp(),
                'end'   => $date->getTimestamp(),
                'allDay' => true,
                'url' => "javascript:Core.Fuel('load','ГСМ за " . $date . "',{id:" . $id . "});",
                'className' => 'new',
                'color' => 'red'
            );
        }
        echo $this->_helper->json($json, true);
    }

    public function addAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $request = $this->getRequest();
        $params = $request->getParams();
        #$this->_helper->layout->disableLayout();
        try {
            $row = $this->_model->fetchRow('id = ' . (int)$params['id'] . ' AND user_id = ' . $this->user->id);
            if($row != null) {
                $row->end = (int)$params['fuel'];
                $row->endtime = date('Y-m-d H:i:s');
            } else {
                $row = $this->_model->createRow();
                $row->start = (int)$params['fuel'];
                $row->starttime = date('Y-m-d H:i:s');
                $row->user_id = $this->user->id;
                $row->end = 0;
                $row->endtime = '0000-00-00 00:00:00';
            }
            $row->save();
            echo $this->_helper->json(array('success' => true, 'result' => '<div class="message success"><span class="strong">УСПЕШНО!</span> Данные ГСМ добавлены</div>'), true);
        } catch (Zend_Db_Exception $e) {
            echo $this->_helper->json(array('success' => false, 'result' => '<div class="message error"><span class="strong">ОШИБКА!</span> ' . $e->getMessage() . '<br /></div>'), true);
        } catch (Zend_Exception $e) {
            echo $this->_helper->json(array('success' => false, 'result' => '<div class="message error"><span class="strong">ОШИБКА!</span> ' . $e->getMessage() . '</div>'), true);
        }
    }

    public function loadAction()
    {
        try {
            $request = $this->getRequest();
            $params = $request->getParams();
            $this->view->fuel = $this->_model->fetchRow('id = ' . (int)$params['id'] . ' AND user_id = ' . $this->user->id);

        } catch (Zend_Exception $e) {
            echo $e->getMessage();
        }
    }

}


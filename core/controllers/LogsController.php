<?php

class LogsController extends Zend_Controller_Action
{

    protected $_grid;

    protected $_model;

    protected $_acl;

    public function init()
    {
        $this->_helper->layout->disableLayout();
        $this->_model = new Core_Model_Authlogs();
        /*$this->_grid = new Core_Model_Grid(
            array(
                'search' => array('user_name')
            )
        );*/
        $this->_acl = Zend_Auth::getInstance();
        #$this->_model->setUser((int)$this->_acl->getIdentity()->id);
        #$this->_helper->viewRenderer->setNoRender();
    }

    public function indexAction()
    {
        /*$request = $this->getRequest();
        $params = $request->getParams();
        $this->_grid->setSelect($this->_model->select());
        $this->_grid->setParams(
            array(
                'search'        => $this->_getParam('search', ''),
                'sortField'     => $this->_getParam('sortField', 'id'),
                'sortDir'       => $this->_getParam('sortOrder', 'desc'),
                'currentPage'   => $this->_getParam('page', 1),
                'itemsPerPage'  => $this->_getParam('show', 20)
            )
        );
        $select = $this->_grid->getSelect();

        if ($request->isXmlHttpRequest()) {

            $result['pager'] = array(array(
                'page'			=>	$pages->current,
                'pages'			=> 	$pages->pageCount,
                'found' 		=>	$pages->totalItemCount,
                'displayingStart'	=> 	($pages->pageCount * ($itemsPerPage - 1) + 1),
                'displayingEnd'		=> 	($pages->pageCount * $itemsPerPage)
            ));

            $result['headings'] = array(
                array(
                    'id'			=>		'id',
                    'display'		=>		'ID',
                    'sort'			=>		'sort-desc'
                ),
                array(
                    'id'			=>		'ip',
                    'display'		=>		'IP'
                ),
                array(
                    'id'			=>		'priorityName',
                    'display'		=>		'Тип'
                ),
                array(
                    'id'			=>		'controller',
                    'display'		=>		'Контроллер'
                ),
                array(
                    'id'			=>		'action',
                    'display'		=>		'Действие'
                ),
                array(
                    'id'			=>		'message',
                    'display'		=>		'Лог'
                ),
                array(
                    'id'			=>		'timestamp',
                    'display'		=>		'Время',
                    'width'			=>		'130px'
                ),
                array(
                    'id'			=>		'ActionBtn',
                    'display'		=>		'Действие',
                    'width'			=>		'130px'
                )
            );

            $rowset = $this->_model->fetchAll($select);
            foreach($rowset as $row)
            {
                $data = $row->toArray();
                $tmp = array();
                foreach($result['headings'] as $arr) {
                    $tmp[$arr['id']] = $data[$arr['id']];
                }
                $tmp['ActionBtn'] = '<a href="#" onClick="javascript:$(this).parent().parent().fadeOut();">test</a>'; //$(this).ctLoad(false, false);
                $result['rows'][] = $tmp;
            }
            #$this->view->transactions = $result;
            $this->_helper->json($result, true);
        }
        $this->view->messages = $this->flash->getMessages();
        $arr = array($request->controller, $request->action);
        foreach($params as $name => $value){ if(!in_array($name, array('controller', 'module', 'action'))){ array_push($arr, $name, $value); }}
        $pages->location = '/' . implode($arr, '/');
        $pages->itemsPerPage = $itemsPerPage;
        $this->view->pages = $pages;*/
        if((int)$this->_acl->getIdentity()->group_id == 1) {
            $this->view->items = $this->_model->getAll();
        } else {
            #$this->_redirect('/index/logout');
        }
    }

}
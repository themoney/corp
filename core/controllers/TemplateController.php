<?php

class TemplateController extends Zend_Controller_Action
{

    protected $_model;

    protected $_acl;

    protected $day_week = array(
        1 => 'Понедельник',
        2 => 'Вторник',
        3 => 'Среда',
        4 => 'Четверг',
        5 => 'Пятница',
        6 => 'Суббота',
        7 => 'Воскресение'
    );

    protected $user;

    public function init()
    {
        /* Initialize action controller here */
        $this->_model = new Core_Model_Templates();
        $this->_acl = Zend_Auth::getInstance();
        $this->user = $this->_acl->getIdentity();
        $this->_helper->layout->disableLayout();
    }

    public function indexAction()
    {
        $request = $this->getRequest();
        $params = $request->getParams();
        if(!empty($params['create'])) {
            try {
                $ids = array();
                $this->_model->getAdapter()->beginTransaction();
                $date = new Zend_Date();
                $day_week = $date->get(Zend_Date::WEEKDAY_DIGIT);
                $templates = $this->_model->getTemplate($day_week);
                $routes = new Core_Model_Routes();
                foreach($templates as $template) {
                    $temp = $template->toArray();
                    unset($temp['id'], $temp['day_week'], $temp['created']);
                    $temp['date'] = $date->toString('YYYY-MM-dd');
                    $temp['time_visit'] = NULL;
                    $temp['result'] = '';
                    $temp['comment'] = '';
                    $row = $routes->createRow();
                    $row->setFromArray($temp);
                    array_push(&$ids, $row->save());
                }
                $this->_model->getAdapter()->commit();
            } catch (Zend_Db_Exception $e) {
                $this->_model->getAdapter()->rollBack();
                echo '<div class="message error"><span class="strong">ОШИБКА!</span> ' . $e->getMessage() . '<br /></div>';
            } catch (Zend_Exception $e) {
                echo '<div class="message error"><span class="strong">ОШИБКА!</span> ' . $e->getMessage() . '</div>';
            }
            echo '<div class="message success"><span class="strong">УСПЕШНО!</span> Создано маршрутных листов на ' . $date->get(Zend_Date::WEEKDAY) . ': ' . count($ids) . '</div>';
            return;
        }
        $this->view->items = $this->_model->getView($this->user);
    }

    public function addAction()
    {
        $request = $this->getRequest();
        $params = $request->getParams();
        $users = new Core_Model_Users();
        $this->view->users = $users->getList($this->user->id);
    }

    public function editAction()
    {
        $request = $this->getRequest();
        $params = $request->getParams();
        $this->view->day_week = (int)$params['day_week'];
        $this->view->zone = (int)$params['zone'];
        $this->view->templates = $this->_model->getTemplate((int)$params['day_week'], (int)$params['zone']);
        $date = new Zend_Date((int)$this->day_week, Zend_Date::WEEKDAY_DIGIT);
        $this->view->day_week_name = $date->get(Zend_Date::WEEKDAY);
        $users = new Core_Model_Users();
        $this->view->zone_name = $users->getList((int)$params['zone'])->current();
    }

    public function saveAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        #$this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $params = $request->getParams();
        try {
            $current = new Zend_Date();
            $this->_model->getAdapter()->beginTransaction();
            $notify = new Core_Model_Notify();

            # Получаем маршрутный лист
            $routes = $this->_model->getTemplate((int)$params['day_week'], (int)$params['zone']);
            #print_r(count($routes));
            #return;
            $ids = array();
            $add = $edit = $delete = 0;
            $filters = array('*' => array('StripTags'));
            $validators = array(
                'client' => array(
                    //'alnum',
                    array('StringLength', 3, 150),
                    'presence' => 'required'
                ),
                'address' => array(
                    array('StringLength', 3, 150),
                    'presence' => 'required'
                )
            );
            $options = array(
                'escapeFilter' => 'StringTrim',
                'missingMessage' => "Не все обязательные поля заполнены!",
                'notEmptyMessage' => "Поле %field%' обязательное для заполнения!"
            );

            # Перебираем маршрутный лист по каждому пункту
            foreach($routes as $row) {
                array_push(&$ids, $row->id);
                # Если данный маршрут не найден в параметрах, значит он был удалён
                if(!in_array($row->id, $params['edit'])) {
                    # Удаляем его в БД
                    $row->delete();
                    $delete++;
                } else {
                    $params['row'][$row->id]['client'] = stripslashes($params['row'][$row->id]['client']);
                    $params['row'][$row->id]['address'] = stripcslashes($params['row'][$row->id]['address']);
                    # Если найден, редактируем
                    $row->setFromArray($params['row'][$row->id]);
                    $row->save();
                    $edit++;
                }
            }
            # Теперь создаём новые
            $users = new Core_Model_Users();
            $city = $users->getCity((int)$params['zone']);
            $clients = new Core_Model_Clients();
            $address = new Core_Model_Address();
            $exists = array();
            foreach($params['new']['client'] as $id => $value) {
                if(empty($params['new']['client'][$id]) || empty($params['new']['address'][$id])) continue;
                $params['new']['client'][$id] = stripslashes($params['new']['client'][$id]);
                $client_id = $clients->add($params['new']['client'][$id]);
                $params['new']['address'][$id] = stripcslashes($params['new']['address'][$id]);
                $address->add($client_id, $params['new']['address'][$id]);
                $row = $this->_model->createRow();
                $row->client = $params['new']['client'][$id];
                $row->address = $params['new']['address'][$id];
                $row->mpz = $params['new']['mpz'][$id];
                $row->proch = $params['new']['proch'][$id];
                $row->sku_mpz = $params['new']['sku_mpz'][$id];
                $row->sku_proch = $params['new']['sku_proch'][$id];
                $row->hp = $params['new']['hp'][$id];
                $row->posom = $params['new']['posom'][$id];
                $row->user_id = $this->user->id;
                $row->day_week = (int)$params['day_week'];
                $row->city = $city;
                $row->zone = (int)$params['zone'];
                $row->save();
                $add++;
            }
            $this->_model->getAdapter()->commit();
            # Если было редактирование существуещего маршрутного листа
            if(count($routes) > 0) {
                $desc = ' Изменено: ' . $edit . ', Добавлено: ' . $add . ', Удалено: ' . $delete;
            } else {
                $desc = ' Добавлено: ' . $add;
            }
            echo $this->_helper->json(array('success' => true, 'result' => '<div class="message success"><span class="strong">УСПЕШНО!</span> Маршрутный лист сохранён.' . $desc . '</div>'), true);
        } catch (Zend_Db_Exception $e) {
            $this->_model->getAdapter()->rollBack();
            echo $this->_helper->json(array('success' => false, 'result' => '<div class="message error"><span class="strong">ОШИБКА!</span> ' . $e->getMessage() . '<br /></div>'), true);
        } catch (Zend_Exception $e) {
            echo $this->_helper->json(array('success' => false, 'result' => '<div class="message error"><span class="strong">ОШИБКА!</span> ' . $e->getMessage() . '</div>'), true);
        }
    }

}


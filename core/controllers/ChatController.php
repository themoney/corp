<?php

class ChatController extends Zend_Controller_Action
{

    protected $_session;

    protected $_model;

    protected $_acl;

    public function init() {
		$this->_helper->layout->disableLayout();
        #$this->_session = new Zend_Session_Namespace('Form_Stats');
        $this->_model = new Core_Model_Chat();
        $this->_acl = Zend_Auth::getInstance();
    }

    public function indexAction() {
        $request = $this->getRequest();
        $params = $request->getParams();
        $this->view->items = $this->_model->getAll();
    }

    public function pmAction() {
        $request = $this->getRequest();
        $params = $request->getParams();
        $this->view->to = (int)$params['id'];
        $users = new Core_Model_Users();
        $this->view->names = $users->getZones();
        $this->view->items = $this->_model->getPM((int)$this->_acl->getIdentity()->id, (int)$params['id']);
    }

    public function sendAction() {
        $this->_helper->viewRenderer->setNoRender();
        $request = $this->getRequest();
        $params = $request->getParams();
        $this->_model->add((int)$this->_acl->getIdentity()->id, $params['msg'], (int)$params['pm']);
        if((int)$params['pm'] > 0) {
            $this->_helper->json($this->_model->getPM((int)$this->_acl->getIdentity()->id, (int)$params['pm'], (int)$params['last'])->toArray(), true);
        }
    }

    public function pollAction() {
        $request = $this->getRequest();
        $params = $request->getParams();
        if((int)$params['pm'] > 0) {
            $this->_helper->json($this->_model->getPM((int)$this->_acl->getIdentity()->id, (int)$params['pm'], (int)$params['last'])->toArray(), true);
        } else {
            $this->_helper->json(array_reverse($this->_model->getAll((int)$params['last'])->toArray()), true);
        }
    }

}


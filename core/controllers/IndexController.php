<?php

class IndexController extends Zend_Controller_Action
{

    protected $_acl;

    public function init()
    {
        /* Initialize action controller here */
        $this->_acl = Zend_Auth::getInstance();
    }

    public function calendarAction()
    {
        $this->_helper->layout->disableLayout();
        $users = new Core_Model_Users();
        $this->view->users = $users->getList((int)$this->_acl->getIdentity()->id);
    }

    public function templateAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->redirector('index', 'template');
    }

    public function notifyAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->redirector('index', 'notify');
    }

    public function logsAction()
    {

        $this->_helper->layout->disableLayout();
        $this->_helper->redirector('index', 'logs');
    }

    public function chatAction()
    {
        $this->_helper->layout->disableLayout();
        #$this->_helper->redirector('index', 'logs');
    }

    public function reportsAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->redirector('index', 'reports');
    }

    public function filesAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->redirector('index', 'files');
    }

    public function monitoringAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->redirector('index', 'monitoring');
    }

    public function indexAction()
    {
        #$acl = $this->_acl->getIdentity();
        #Zend_Debug::dump($acl); exit();
        #$acl = Zend_Registry::get('acl');
        #Zend_Debug::dump($acl->getRoleName());
    }
    
    /*public function passAction()
    {
        $request = $this->getRequest();
        $params = $request->getParams();
        $params['login'] = strtolower($params['login']);
        #$params['password'] = strtolower($params['password']);
        exit(md5($params['login'] . strlen($params['login'] . $params['password']) . $params['password']));
    }

    public function passlistAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $list = array(
            'c1' => 'iG1sLU16',
            'c2' => 'VpS77401',
            'c3' => 'Q4al818d',
            'c4' => '774sD5c2',
            'c5' => 'r9858Hhl',
            't1' => 'h2v7DcJJ',
            't2' => '573l5hoR',
            't3' => '53x5L8c5',
            't4' => 'bC519knB',
            't5' => '52l3MR6G',
            'stlt' => 'XW71IJ4y',
            'ssmr' => 'VyV2jt6g'
        );
        foreach($list as $login => $password) {
            echo $login . ":" . md5($login . strlen($login . $password) . $password) . "<br />";
        }
    }*/

    public function loginAction()
    {
        $request = $this->getRequest();
        $params = $request->getParams();
        if ($request->isPost()) {

            if(empty($params['login']) || empty($params['password'])) {
                $this->_helper->flashMessenger->setNamespace('error')->addMessage('Заполните все поля');
                $this->_helper->redirector('index');
            }
            
            $params['login'] = strtolower($params['login']);

            $adapter = new Zend_Auth_Adapter_DbTable(
                Zend_Registry::get('db'),
                'users',
                'login',
                'password',
                'MD5(CONCAT(login,LENGTH(CONCAT(login,?)),?))'
            );

            $adapter->setIdentity($params['login']);
            $adapter->setCredential($params['password']);
            $adapter->getDbSelect()->joinLeft('groups', 'groups.id = users.group_id', array('group_name' => 'name'));
            $acl = Zend_Registry::get('acl');
            $result = $this->_acl->authenticate($adapter);

            switch($result->getCode()) {

                case Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND:
                    $this->_helper->flashMessenger->setNamespace('error')->addMessage('Логин не найден');
                break;

                case Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID:
                    $this->_helper->flashMessenger->setNamespace('error')->addMessage('Неверный пароль');
                break;

                case Zend_Auth_Result::FAILURE_IDENTITY_AMBIGUOUS:
                    $this->_helper->flashMessenger->setNamespace('error')->addMessage('Найдено более одной записи');
                break;

                case Zend_Auth_Result::SUCCESS:
                    $row = $adapter->getResultRowObject(null, 'password');
					#Zend_Session::rememberMe(60);
					$this->_acl->getStorage()->write($row);
                    $authlogs = new Core_Model_Authlogs();
                    $authlogs->write($row->id);
                    unset($authlogs);
                break;

                case Zend_Auth_Result::FAILURE:
                    $this->_helper->flashMessenger->setNamespace('error')->addMessage('Ошибка при проверке данных');
                break;

                default:
                    $this->_acl->clearIdentity();
                    $this->_helper->flashMessenger->setNamespace('error')->addMessage('Неизвестная операция');
                break;

            }
            $this->_helper->redirector('index');
        }
    }

    public function logoutAction()
    {
        Zend_Auth::getInstance()->clearIdentity();
        $this->_helper->redirector('index');
    }

}


<?php

class UsersController extends Zend_Controller_Action
{

    protected $_model;

    public function init()
    {
        /* Initialize action controller here */
        $this->_model = new Core_Model_Users();
    }

    public function indexAction()
    {
        $page = $this->_getParam('page', 1);
        $sortField = $this->_getParam('sort', 'id');
        $sortDir = $this->_getParam('dir', 'asc');
        switch ($sortDir) {
            case 'asc':
                $sortDir = 'asc';
            break;

            default:
                $sortDir = 'desc';
            break;
        }
        $result = $this->_model->view($page, $sortField, $sortDir);
        $this->view->items = $result;
    }

    public function addAction()
    {
        $request = $this->getRequest();
        $params = $request->getParams();
        if($request->isPost()) {
            $this->_model->getAdapter()->beginTransaction();
            try {
                $row = $this->_model->createRow();
                $row->setFromArray($params);
                $row->group_id = (int)$params['group'];
                $row->password = md5(base64_encode(strlen($params['login']) . $params['login'] . strlen($params['password']) . $params['password']));
                $row->registration = new Zend_Db_Expr('NOW()');
                $row->save();
                $id = $this->_model->getAdapter()->lastInsertId();
                $data = new Core_Model_UsersData();
                foreach($params as $name => $value) {
                    if(in_array($name, $data->fields)) {
                        $row = $data->createRow();
                        $row->user_id = $id;
                        $row->name = $name;
                        $row->value = $value;
                        $row->save();
                    }
                }
                $this->_model->getAdapter()->commit();
                $this->_helper->flashMessenger->setNamespace('success')->addMessage('Партнёр успешно создан');
                $this->_helper->redirector('index');
            } catch(Zend_Db_Exception $e) {
                $this->_model->getAdapter()->rollBack();
                $this->_helper->flashMessenger->setNamespace('error')->addMessage('Партнёр не создан!');
                #$this->_helper->redirector('index');
            }
            #$this->_helper->redirector('edit', 'services', null, array('id' => $id, 'hash' => Zend_Session::getId()));
        }
        $form = $this->_model->getForm();
        $this->view->form = $form;
    }

    public function deleteAction()
    {
        $id = $this->_request->getParam('id', false);
        $hash = $this->_request->getParam('hash', false);
        if($id !== false && $hash == Zend_Session::getId()) {
            $data = new Core_Model_UsersData();
            if($this->_model->remove($id)) {
                $this->_helper->flashMessenger->setNamespace('success')->addMessage('Партнёр ID#' . $id . ' успешно удалён');
            } else {
                $this->_helper->flashMessenger->setNamespace('error')->addMessage('Партнёр не может быть удалён!');
            }
        } else {
            $this->_helper->flashMessenger->setNamespace('warning')->addMessage('Ошибка в переданных параметрах');
        }
        $this->_helper->redirector('index');
    }

//    public function editAction()
//    {
//        $id = $this->_request->getParam('id', false);
//        $hash = $this->_request->getParam('hash', false);
//        if($id !== false && $hash == Zend_Session::getId()) {
//            if($row = $this->_model->find($id)) {
//                $row = $row->current();
//                $request = $this->getRequest();
//                $params = $request->getParams();
//                if($request->isPost()) {
//                    /*
//                     * Работа с префиксами если они заданы
//                     */
//                    # Создание
//                    $db = Zend_Registry::get('db');
//                    if(!empty($params['prefix2']) && (strlen($params['prefix2']) < 3 || empty($params['prefix1']))) {
//                        $this->_helper->flashMessenger->setNamespace('warning')->addMessage('Ошибка в переданных параметрах префикса');
//                        $this->_helper->redirector('edit', 'services', null, array('id' => $id, 'hash' => $hash));
//                    } elseif(!empty($params['prefix2'])) {
//                        $select = $db->select();
//                        $select->from('prefix__1', array('prefix'))->where('id = ?', (int)$params['prefix1'])->limit(1);
//                        $prefix1 = $db-> fetchRow($select);
//                        $select = $db->select();
//                        $select->from('prefixes',array(
//                                "c" => new Zend_Db_Expr('count(`prefix`)')
//                            ))
//                            ->where("available = ?", 1)
//                            ->where("prefix = ?", new Zend_Db_Expr("LEFT(" . strtolower($prefix1['prefix'] . $params['prefix2']) . ",CHAR_LENGTH(prefix))"))
//                            ->limit(1);
//                        $p = $db-> fetchRow($select);
//                        if((bool)$p['c']) {
//                            $this->_helper->flashMessenger->setNamespace('warning')->addMessage('Префикс недоступен или занят');
//                            $this->_helper->redirector('edit', 'services', null, array('id' => $id, 'hash' => $hash));
//                        } else {
//                            $db->insert('prefix__2', array(
//                                'user_id'       => (int)$this->_acl->getIdentity()->id,
//                                'service_id'    => (int)$id,
//                                'prefix_id'     => (int)$params['prefix1'],
//                                'prefix'        => strtolower($params['prefix2'])
//                            ));
//                            $this->_helper->flashMessenger->setNamespace('success')->addMessage('Новый префикс успешно добавлен');
//                        }
//                    }
//                    # Удаление
//                    if(!empty($params['prefixes']) && is_array($params['prefixes'])) {
//                        $db->delete('prefix__2', $db->quoteInto('id IN (?)', $params['prefixes']));
//                        $this->_helper->flashMessenger->setNamespace('success')->addMessage('Выбранные префиксы успешно удалены');
//                    }
//                    /*
//                     * Сохранение значений сервиса
//                     */
//                    $row->setFromArray($params);
//                    $row->save();
//                    $this->_helper->flashMessenger->setNamespace('success')->addMessage('Сервис успешно изменён');
//                    $this->_helper->redirector('edit', 'services', null, array('id' => $id, 'hash' => $hash));
//                }
//                $form = $this->_model->getForm($row);
//                $this->view->form = $form;
//            } else {
//                $this->_helper->flashMessenger->setNamespace('error')->addMessage('Сервис ID#' . $id . ' не найден');
//                $this->_helper->redirector('index');
//            }
//        } else {
//            $this->_helper->flashMessenger->setNamespace('warning')->addMessage('Ошибка в переданных параметрах');
//            $this->_helper->redirector('index');
//        }
//    }

}


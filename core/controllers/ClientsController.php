<?php

class ClientsController extends Zend_Controller_Action
{

    protected $_model;

    protected $_acl;

    public function init()
    {
        /* Initialize action controller here */
        $this->_model = new Core_Model_Clients();
        #$this->_acl = Zend_Auth::getInstance();
        #$this->_model->setUser((int)$this->_acl->getIdentity()->id);
        $this->user = new stdClass();
        $this->user->id = 4;
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
    }

    public function indexAction()
    {
        $term = $this->_getParam('term');
        echo $this->_helper->json($this->_model->autocomplete($term), true);
    }

}
<?php

class ErrorController extends Zend_Controller_Action
{

    public function errorAction()
    {
        $errors = $this->_getParam('error_handler');
        $this->_helper->layout->disableLayout();
        $ajax = $this->getRequest()->isXmlHttpRequest();
        if(APPLICATION_ENV != "production" && !$ajax) {
		    $this->_helper->viewRenderer('debug');
        } elseif($ajax) {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
        }
        if (!$errors) {
            $this->view->message = 'Что-то пошло не так, попробуйте ещё раз';
            return;
        }
        
        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);
                $this->view->message = 'Такой страницы не существует';
                break;
            default:
                // application error
                $this->getResponse()->setHttpResponseCode(500);
                $this->view->message = 'Ошибка в работе платформы';
                break;
        }
        
        // Log exception, if logger available
        if ($log = $this->getLog()) {
            $log->crit($this->view->message . "\n" . $errors->exception->getMessage() . "\n" . $errors->exception->getTraceAsString(), $errors->exception);
        }
        
        // conditionally display exceptions
        if ($this->getInvokeArg('displayExceptions') == true) {
            $this->view->exception = $errors->exception;
        }
        $this->view->request   = $errors->request;
    }

    public function deniedAction() {
        if($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $this->getResponse()->setHttpResponseCode(401);
        }
        $this->view->message = $this->getRequest()->getParam('message');
    }

    public function getLog()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
        if (!$bootstrap->hasResource('Log')) {
            return false;
        }
        $log = $bootstrap->getResource('Log');
        return $log;
    }


}


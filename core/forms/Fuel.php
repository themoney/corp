<?php

class Core_Form_Fuel extends Zend_Form
{

    public function init()
    {
        $this->setAction('/reports/fuel');
        $this->setMethod('post');
        $this->setAttrib('id', 'fuelform');
        $this->clearDecorators();
        $this->setElementDecorators(array(
            //array('PrepareElements'),
            array('ViewHelper'),
            //array('Errors', array('tag' => 'div')),
            //array('Description', array('tag' => 'label')),
            array('Label')
            //array('HtmlTag', array('tag' => 'span'))
        ));

        $db = Zend_Registry::get('db');

        // Генерируем список зон
        $this->addElement('select', 'zone', array(
            'label' => 'Зона',
            //'class' => 'select',
            'multiOptions' => array('' => '--- ВСЕ ---')
        ));
        $select = $db->select();
        #$select->setIntegrityCheck(false);
        $select->from('fuel', array('user_id'))
            ->join('users', 'users.id = fuel.user_id', array('name'))
            ->group("user_id")
            ->order("name ASC");
        $rowset = $db->fetchAll($select);
        foreach ($rowset as $row) $this->zone->addMultiOption($row['user_id'], $row['name']);

        $this->addElement(
            'text', 'from', array(
            'id'    => 'fdatefrom',
            'value' => date('Y-m-d'),
            'label' => 'Начало',
            'maxlength' => 10,
            'minlength' => 10,
            'size'      => 10,
            'filters'    => array('StringTrim')
        ));

        $this->addElement(
            'text', 'to', array(
            'id'    => 'fdateto',
            'value' => date('Y-m-d'),
            'label' => 'Окончание',
            'maxlength' => 10,
            'minlength' => 10,
            'size'      => 10,
            'filters'    => array('StringTrim')
        ));

        $this->addElement('button', 'fsubmit', array(
            'ignore'   => true,
            //'order'    => -1,
            'label'    => 'Сформировать',
        ));
        $this->fsubmit->setDecorators(array('ViewHelper'));
        $this->addDisplayGroup(
            array('zone', 'from', 'to', 'fsubmit'), 'fdate',
            array(
                'legend' => 'Отчёт ГСМ'
            )
        );

    }

}


<?php

class Core_Form_Monitoring extends Zend_Form
{

    public function init()
    {
        $this->setAction('/reports/graph');
        $this->setMethod('post');
        $this->setAttrib('id', 'monitoringform');
        $this->clearDecorators();
        $this->setElementDecorators(array(
            //array('PrepareElements'),
            array('ViewHelper'),
            //array('Errors', array('tag' => 'div')),
            //array('Description', array('tag' => 'label')),
            array('Label')
            //array('HtmlTag', array('tag' => 'span'))
        ));

        $db = Zend_Registry::get('db');

        // Список типов
        $this->addElement('select', 'product', array(
            'label' => 'Продукт',
            //'class' => 'select',
            'multiOptions' => array('' => '--- ВСЕ ---')
        ));

        $select = $db->select()->from('m_products')->order('name ASC');
        $rowset = $db->fetchAll($select);
        foreach ($rowset as $row) $this->product->addMultiOption($row['id'], $row['name']);

        $this->addElement(
            'text', 'mfrom', array(
            'id'    => 'mdatefrom',
            'value' => date('Y-m-d'),
            'label' => 'Начало',
            'maxlength' => 10,
            'minlength' => 10,
            'size'      => 10,
            'filters'    => array('StringTrim')
        ));

        $this->addElement(
            'text', 'mto', array(
            'id'    => 'mdateto',
            'value' => date('Y-m-d'),
            'label' => 'Окончание',
            'maxlength' => 10,
            'minlength' => 10,
            'size'      => 10,
            'filters'    => array('StringTrim')
        ));

        $this->addElement('submit', 'msubmit', array(
            'ignore'   => true,
            //'order'    => -1,
            'label'    => 'Сформировать',
        ));
        $this->msubmit->setDecorators(array('ViewHelper'));
        $this->addDisplayGroup(
            array('product', 'mfrom', 'mto', 'msubmit'), 'mdate',
            array(
                'legend' => 'График мониторинга'
            )
        );

    }

}


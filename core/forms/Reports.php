<?php

class Core_Form_Reports extends Zend_Form
{

    public function init()
    {
        $this->setAction('/reports/pdf');
        $this->setMethod('post');
        $this->setAttrib('id', 'reportform');
        $this->clearDecorators();
        $this->setElementDecorators(array(
            //array('PrepareElements'),
            array('ViewHelper'),
            //array('Errors', array('tag' => 'div')),
            //array('Description', array('tag' => 'label')),
            array('Label')
            //array('HtmlTag', array('tag' => 'span'))
        ));

        $db = Zend_Registry::get('db');

        /* Группировка
        $this->addElement('MultiCheckbox', 'groupby', array(
            //'label' => 'Тип клиента',
            //'class' => 'field checkbox',
            'separator' => '',
            'multiOptions' => array(
                'city' => 'По городу',
                'client' => 'По контрагенту',
                'zone' => 'По зоне'
            )
        ));

        /*$this->addDisplayGroup(
            array('groupby'), 'group'
        );*/

        // Список типов
        $this->addElement('select', 'type', array(
            'label' => 'Тип клиента',
            //'class' => 'select',
            'multiOptions' => array('' => '--- ВСЕ ---', 'Н' => 'Новые (H)', 'Р' => 'Рабочие (Р)')
        ));

        // Генерируем список городов
        $this->addElement('select', 'city', array(
            'label' => 'Город',
            //'class' => 'select',
            'multiOptions' => array('' => '--- ВСЕ ---')
        ));

        $select = $db->select();
        #$select->setIntegrityCheck(false);
        $select->from('routes', array('city'))
                ->group("city")
                ->order("city ASC");
        $rowset = $db->fetchAll($select);
        foreach ($rowset as $row) $this->city->addMultiOption($row['city'], $row['city']);

        // Генерируем список контрагентов
        $this->addElement('select', 'client', array(
            'label' => 'Контрагент',
            //'class' => 'select',
            'width'      => 150,
            'multiOptions' => array('' => '--- ВСЕ ---')
        ));
        $select = $db->select();
        #$select->setIntegrityCheck(false);
        $select->from('routes', array('client'))
                ->group("client")
                ->order("client ASC");
        $rowset = $db->fetchAll($select);
        foreach ($rowset as $row) $this->client->addMultiOption($row['client'], $row['client']);

        // Генерируем список зон
        $this->addElement('select', 'zone', array(
            'label' => 'Зона',
            //'class' => 'select',
            'multiOptions' => array('' => '--- ВСЕ ---')
        ));
        $select = $db->select();
        #$select->setIntegrityCheck(false);
        $select->from('routes', array('zone'))
                ->join('users', 'users.id = routes.zone', array('name'))
                ->group("zone")
                ->order("users.name ASC");
        $rowset = $db->fetchAll($select);
        foreach ($rowset as $row) $this->zone->addMultiOption($row['zone'], $row['name']);

        $this->addElement(
            'text', 'from', array(
            'id'    => 'datefrom',
            'value' => date('Y-m-d'),
            'label' => 'Начало',
            'maxlength' => 10,
            'minlength' => 10,
            'size'      => 10,
            'filters'    => array('StringTrim')
        ));

        $this->addElement(
            'text', 'to', array(
            'id'    => 'dateto',
            'value' => date('Y-m-d'),
            'label' => 'Окончание',
            'maxlength' => 10,
            'minlength' => 10,
            'size'      => 10,
            'filters'    => array('StringTrim')
        ));

        $this->addElement('button', 'preview', array(
            'ignore'   => true,
            //'order'    => -1,
            'label'    => 'Сформировать'
        ));
        $this->preview->setDecorators(array('ViewHelper'));

        $this->addElement('submit', 'pdf', array(
            'ignore'   => true,
            //'order'    => -1,
            'label'    => 'Сохранить в PDF'
        ));
        $this->pdf->setDecorators(array('ViewHelper'));

        $this->addDisplayGroups(array(
            $this->addDisplayGroup(
                array('type', 'city', 'client'), 'where',
                array(
                    'legend' => 'Отчёты по маршутным листам'
                )
            ),
            $this->addDisplayGroup(
                array('zone', 'from', 'to', 'preview', 'pdf'), 'date'
            )
        ));




    }

}


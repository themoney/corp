/**
 * Created by JetBrains PhpStorm.
 * User: intech
 * Date: 04.05.11
 * Time: 5:40
 * To change this template use File | Settings | File Templates.
 */

// date localization for locale 'ru-RU', utf-8 encoding
// provided by Sergey Nechaev http://nechaev.org/)
Date.dayNames = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];
Date.abbrDayNames = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
Date.monthNames = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
Date.abbrMonthNames = ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'];

Date.firstDayOfWeek = 1;
Date.format = 'dd.mm.yyyy';

/* Russian (UTF-8) initialisation for the jQuery UI date picker plugin. */
/* Written by Andrew Stromnov (stromnov@gmail.com). */
$.datepicker.regional['ru'] = {
    closeText: 'Закрыть',
    prevText: '&#x3c;Пред',
    nextText: 'След&#x3e;',
    currentText: 'Сегодня',
    monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
    'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
    monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
    'Июл','Авг','Сен','Окт','Ноя','Дек'],
    dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
    dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
    dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
    weekHeader: 'Не',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
$.datepicker.setDefaults($.datepicker.regional['ru']);

function formatSecondsAsTime(mins, format) {
  var hr  = Math.floor(mins / 60);
  var min = Math.floor(mins - (hr * 60));
  if (hr < 10)   { hr    = "0" + hr; }
  if (min < 10) { min = "0" + min; }
  if (hr>=24)            { return "больше суток"; }
  return hr + ':' + min;
}

var Core = {
    PollCounter: 0,
    History: []
};

(function($) {

    Core.Init = function() {

        $('input,textarea,select,fieldset,legend').addClass('ui-widget-content ui-corner-all');

        var buttons = $("button");
        $.each(buttons, function() {
            if ($(this).hasClass("ui-button")){ return; }
            var icon = $(this).attr("icon");
            if (icon != null) {
                var html = $(this).html();
                if (html.length == 0){
                    var text = false;
                    $(this).html("button");
                }
                else var text = true;

                $(this).button({ icons:{primary:icon}, text:text});
                if (text == false) $(this).removeAttr("title");
                return;
            }
            else $(this).button();
        });

        /*var buttons = $("input[type=checkbox]");
        $.each(buttons, function() {
            if ($(this).hasClass("ui-button")){ return; }
            var icon = $(this).attr("icon");
            var id = $(this).attr("id");
            if (icon != null) {
                var html = $('label[for=' + id + ']').html();
                if (html.length == 0){
                    var text = false;
                    $(this).html("button");
                }
                else var text = true;

                $(this).button({ icons:{primary:icon}, text:text});
                if (text == false) $(this).removeAttr("title");
                return;
            }
            else $(this).button();
        });*/

        $('.ui-dialog-buttonpane button span').each(function(){
            var html = $(this).html();
            switch(html) {
                case 'Удалить':
                    $(this).parent()
                            .removeClass('ui-button-text-only')
                            .addClass('ui-button-text-icon-primary')
                            .prepend('<span class="ui-button-icon-primary ui-icon ui-icon-trash"></span>');
                break;

                case 'Отмена':
                    $(this).parent()
                            .removeClass('ui-button-text-only')
                            .addClass('ui-button-text-icon-primary')
                            .prepend('<span class="ui-button-icon-primary ui-icon ui-icon-cancel"></span>');
                break;

                case 'Сохранить':
                    $(this).parent()
                            .removeClass('ui-button-text-only')
                            .addClass('ui-button-text-icon-primary')
                            .prepend('<span class="ui-button-icon-primary ui-icon ui-icon-disk"></span>');
                break;
            }
        });
        // Tables controls:
        /*$("td.controls a").hide();
        $("tr").hover(
                function () {
                    $(this).find("td.controls a").show();
                },
                function () {
                    $(this).find("td.controls a").hide();
                }
        );*/
        // Alternating table rows:
        $('tbody tr:even').addClass("alt_row"); // Add class "alt-row" to even table rows

        // Check all checkboxes when the one in a table head is checked:
        $('.check_all').click(
                function() {
                    $(this).parent().parent().parent().parent().find("input[type='checkbox'].action").attr('checked', $(this).is(':checked'));
                }
        );

        /* Side Navigation Menu Slide
        $("#nav > li > a.collapsed + ul").slideToggle("medium");
        $("#nav > li > a").click(function() {
            $(this).toggleClass("expanded").toggleClass("collapsed").find("+ ul").slideToggle("medium");
        });*/

        /* Charting script
        $('table.pie').visualize({type: 'pie', height: '300px', width: '620px'});
        $('table.bar').visualize({type: 'bar', height: '300px', width: '620px'});
        $('table.area').visualize({type: 'area', height: '300px', width: '620px'});
        $('table.line').visualize({type: 'line', height: '300px', width: '620px'});

        // Tab Switching
        $("#tabs, #graphs").tabs();

        // Select all checkboxes
        $("#checkboxall").click(function() {
            var checked_status = this.checked;
            $("input[name=checkall]").each(function() {
                this.checked = checked_status;
            });
        });

        // Rich text editor/WYSIWYG
        $('#wysiwyg').wysiwyg();

        */

        Core.Loader.hide();
        $.ajaxSetup({
            cache: false,
            statusCode: {
                401: function() { window.location.replace("/"); }
            },
            beforeSend: Core.Loader.show,
            ajaxComplete: Core.Loader.hide,
            complete: Core.Loader.hide,
            ajaxSuccess: Core.Loader.hide,
            success: Core.Loader.hide,
            //ajaxError: Core.Loader.hide,
            error:  function(xhr, ajaxOptions, thrownError) { Core.Loader.hide(); Core.Message("Ваш запрос не был обработан сервером, данные могут быть потеряны!<br />Попробуйте повторить запрос.<br />" + xhr.responseText, {autoHide: false}); },
            ajaxError: function(xhr) { Core.Loader.hide(); Core.Message("Ваш запрос не был обработан сервером, данные могут быть потеряны!<br />Попробуйте повторить запрос.<br />" + xhr.responseText, {autoHide: false}); }
        });
    };

    Core.Menu = function() {
        Core.History.push($.History.getHash());
        $.History.bind(function(state) {
            $("#nav > li > ul.navigation").children('li:has(a[href="#'+state+'"])').removeClass('hover').addClass('selected').siblings('.selected').removeClass('selected').addClass('hover');
            $.get('/index/'+state, function(data) {
                $(".contentcontainer").html(data);
                Core.Init();
            }).error(function(data){ $(".contentcontainer").html(data.responseText); });
        });
        /*$("#nav > li > ul.navigation > li > a").click(function() {
        });*/
    }

    Core.Message = function(message, options) {
        $(window).humanMsg(message, options);
    }

    // Global loader page
    Core.Loader = $.extend({}, {
        show: function(){
            $('#globalmask,#preloader').remove();
            $('body').append('<div id="globalmask"></div><div id="preloader">Ожидайте...</div>');
        },
        hide: function(){
            $('#preloader,#globalmask').fadeOut();
        }
    });

    Core.Notify = function(text) {
		$('#notifypanel').slideDown();
		$('p.notifycount').html(text).blink();
		$.favicon('/core/images/bg_notify_count.png', function (ctx) {
		  ctx.font = 'bold 22px Arial, "helvetica", sans-serif';
		  ctx.fillStyle = '#FFFFFF';
		  if(text < 10) {
			  ctx.fillText(text, 12, 23);
		  } else {
			  ctx.fillText(text, 5, 23);
		  }
		});
        $("#notifypanel").css('cursor','pointer').click(function() {
			$('#notifypanel').slideUp();
			$.favicon('/core/images/favicon.ico');
            window.location = '#notify';
            return false
        });
    }

    Core.PollChat = function() {
        $.ajax({
            url: '/chat/poll/last/' + Core.PollCounter,
            dataType: 'json',
            beforeSend: function() {},
            success: function(data) {
                data.forEach(function(msg) {
                    $('#chatpanel').append($('<div/>').attr('class', 'msg flash').append($('<div/>').attr('class', 'ui-widget-header head').html(msg.name + ':').append($('<span/>').html(msg.date))).append($('<p/>').attr('class', 'ui-widget-content').html(msg.text)));
                    Core.PollCounter = msg.id;
                });
                $('#chatpanel').scrollTop(1000);
                $('#chatpanel > div.flash').removeClass('flash').show('pulsate');
                setTimeout(Core.PollChat, 10000);
            }
        });
    }

    Core.PollPMChat = function() {
        $.ajax({
            url: '/chat/poll/last/' + Core.PollCounter,
            dataType: 'json',
            beforeSend: function() {},
            success: function(data) {
                data.forEach(function(msg) {
                    $('#chatpanel').append($('<div/>').attr('class', 'msg flash').append($('<div/>').attr('class', 'ui-widget-header head').html(msg.name + ':').append($('<span/>').html(msg.date))).append($('<p/>').attr('class', 'ui-widget-content').html(msg.text)));
                    Core.PollCounter = msg.id;
                });
                $('#chatpanel').scrollTop(1000);
                $('#chatpanel > div.flash').removeClass('flash').show('pulsate');
                setTimeout(Core.PollChat, 10000);
            }
        });
    }

    Core.PollNotify = function() {
        $('#calendar').fullCalendar('refetchEvents');
        $.get('/notify/poll', function(data){
            if(data.count > 0) { Core.Notify(data.count); } else { $('#notifypanel').slideUp(); $.favicon('/core/images/favicon.ico'); }
        });
        setTimeout(Core.PollNotify, 60000);
    }

    Core.PollOnline = function() {
        $.get('/notify/online', function(data) {
            $('#chatusers > ul').remove();
            $('#chatusers').append($('<ul/>'));
            var notify = 0;
            data.forEach(function(user) {
                online = parseInt(user.online);
                unread = parseInt(user.unread);
                notify+=unread;
                BtnUser = $('<span/>').width(402).button({label: '[' + user.city + '] ' + user.group + ' ' + user.name, icons: ((online < 10) ? {primary:'ui-icon-flag'} : {})}).addClass(((online < 10) ? 'online' : '')).attr('title', ((!isNaN(online)) ? formatSecondsAsTime(online) : '--:--:--') + ' назад').click(function(){ Core.PM(user.id); });
                BtnMail = $('<span/>').button({label: unread, icons: { primary: 'ui-icon-mail-closed'}}).addClass(((unread > 0) ? 'newmsg' : '')).click(function(){ Core.PM(user.id); });
                //BtnTask = $('<span/>').button({label: '', icons: { secondary: 'ui-icon-clipboard', primary: 'ui-icon-clipboard'}});
                item = $('<li/>').append(BtnUser)
                        //.append(BtnTask)
                        .append(BtnMail).buttonset();
                $('#chatusers > ul').append(item);
            });
            if(notify > 0) {
                $('li.btnusers').button('option', {icons: { primary: 'ui-icon-triangle-1-w', secondary: 'ui-icon-mail-closed'}}).show('pulsate');
            } else {
                $('li.btnusers').button('option', {icons: { primary: 'ui-icon-triangle-1-w', secondary: false}});
            }
        });
        setTimeout(Core.PollOnline, 60000);
    }

    Core.PM = function(id) {
            $('div[id*="pm"]').dialog('close');
            return $('<div/>',{id:'pm'+id, 'class':'formContainer'}).dialog({
                title: 'Загрузка...',
                resizable: false,
                width: 580,
                minHeight: 450,
                //position: [(150 * $('div[id*="pm"]').length),'bottom'],
                modal: false,
                open: function() {
                    var form = $(this);
                    $.get('/chat/pm/id/' + id, {}, function(data) {
                        form.html(data);
                    });
                },
                close: function() {
                    $(this).remove();
                }
            });
    }

    Core.Task = function(action, title, opt) {
            return $('<div/>',{id:'taskform', 'class':'formContainer'}).dialog({
                title: title,
                resizable: false,
                width: 580,
                minHeight: 450,
                //position: 'top',
                modal: true,
                open: function() {
                    var form = $(this);
                    $.get('/task/' + action, opt, function(data) {
                        form.html(data);
                    });
                },
                close: function() {
                    $(this).remove();
                }
            });
    }

    Core.Route = function(action, title, opt) {
            return $('<div/>',{id:'routeform', 'class':'formContainer'}).dialog({
                title: title,
                resizable: false,
                width: 1000,
                height: 450,
                //position: 'top',
                modal: true,
                open: function() {
                    var form = $(this);
                    $.get('/route/' + action, opt, function(data) {
                        form.html(data);
                    });
                },
                close: function() {
                    $(this).remove();
                }
            });
    }

    Core.Template = function(action, title, opt) {
            return $('<div/>',{id:'templateform', 'class':'formContainer'}).dialog({
                title: title,
                resizable: false,
                width: 1000,
                height: 450,
                //position: 'top',
                modal: true,
                open: function() {
                    var form = $(this);
                    $.get('/template/' + action, opt, function(data) {
                        form.html(data);
                    });
                },
                close: function() {
                    $(this).remove();
                }
            });
    }

    Core.Fuel = function(action, title, opt) {
        return $('<div/>',{id:'fuelform', 'class':'formContainer'}).dialog({
            title: title,
            resizable: false,
            width: 580,
            minHeight: 450,
            //position: 'top',
            modal: true,
            open: function() {
                var form = $(this);
                $.get('/fuel/' + action, opt, function(data) {
                    form.html(data);
                });
            },
            close: function() {
                $(this).remove();
            }
        });
    }

})(jQuery);

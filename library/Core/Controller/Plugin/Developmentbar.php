<?php
/**
 * Development Bar 
 *
 * This plugin will add a little bar to the bottom of your webpage
 * while you browse your webpage in development mode.
 * 
 * It will show you:
 * - Current ZF version that is in use.
 * - Peak amount of memory used for the current page.
 * - Avarage dispatch time for the last 100 page views.
 * - The amount of time the current dispatch is slower 
 *   or faster than the avarage 100 last dispatches.
 * - Amount and information about known Cookies
 * - Number of executed SQL-queries and the time it took
 * - Number of SQL-queries executed per second
 * - The longest SQL-query in a javascript alert window
 *  
 * @todo Use a canvas to generate the arrows. 
 * @todo Add a log reader that tails the error log for -n lines
 * @todo Organize categories in folding tabs
 *
 * @uses      Zend_Controller_Plugin_Abstract
 * @category  My
 * @package   My_Controller
 * @copyright Copyright (C) 2009 - Mathias Johansson
 * @author    Mathias Johansson <hi@mathiasjohansson.se>
 * @link      http://www.mathiasjohansson.se
 * @license   New BSD {@link http://framework.zend.com/license/new-bsd}
 * @version   $Id: $
 */
class Core_Controller_Plugin_Developmentbar extends Zend_Controller_Plugin_Abstract
{
    private $_iStart = 0;
    private $_iStop = 0;
    private $_oNs = null;
    
    // Workaround due to a bug in php 5.2.2 where one can 
    // not indirectly use namespaced arrays
    private $_aExecTime = array();
    
    // Prior to PHP 5.2.1, to use this directive you need to add 
    // -enable-memory-limit in the configure line at compile time.         	    
    private $iPeakUsage = 0;
    private $_oDb = null;
    
    private $_iAvarageOf = 100;

    public function __construct() {
        
    	if (!Zend_Session::isStarted()) {
    		Zend_Session::start();
    	}
    	
        $this->_oNs = new Zend_Session_Namespace('devNamespace');

        if (isset($this->_oNs->aExecTime)) {
           	$this->_aExecTime = $this->_oNs->aExecTime;
        }    	
    }
    
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        $this->_iStart = microtime(true);
        $this->_oDb = Zend_Registry::get('db');
        $this->_oDb->getProfiler()->setEnabled(true); 
        
    }
 	
    public function postDispatch(Zend_Controller_Request_Abstract $request)
    {
        if(!$this->getRequest()->isXmlHttpRequest()) {

        	// Get peak usage of memory used
        	$this->iPeakUsage = memory_get_peak_usage(true);    
        	
            // Get execution time for this dispatch
            $this->_iStop = microtime(true);            
            $iExecTime = $this->_iStop - $this->_iStart;

            // Calculate avarage execution time 
            array_push($this->_aExecTime, $iExecTime);            
            if(count($this->_aExecTime) > $this->_iAvarageOf) {
                array_shift($this->_aExecTime);
            }
            
            $iAvarageExecTime = 0;
            foreach($this->_aExecTime as $iTime) {
                $iAvarageExecTime += $iTime;
            }
            
            $this->_oNs->aExecTime = $this->_aExecTime;
            
            $iAvarageExecTime = $iAvarageExecTime/count($this->_aExecTime);            
            $iExecDiff = $iExecTime - $iAvarageExecTime;

            // Calculate sql information
            $oProfiler = $this->_oDb->getProfiler();
    
            $totalTime            = $oProfiler->getTotalElapsedSecs();
            $queryCount           = $oProfiler->getTotalNumQueries();
            $longestTime          = 0;
            $avarageQueryTime     = 0;
            $queriesPerSecond     = 0;
            $longestQuery         = null;
            if ($oProfiler->getQueryProfiles()) {

                foreach ($oProfiler->getQueryProfiles() as $oQuery) {
		            if ($oQuery->getElapsedSecs() > $longestTime) {
                        $longestTime  = $oQuery->getElapsedSecs();
                        $longestQuery = $oQuery->getQuery();
                    }
                    #Zend_Debug::dump($oQuery);
                    #Zend_Debug::dump($oQuery->getId());
                }
        
                $avarageQueryTime = $totalTime / $queryCount;
                $queriesPerSecond = $queryCount / $totalTime;
            }
            $firephp = Zend_Wildfire_Plugin_FirePhp::getInstance();
            $firephp->send(Zend_Version::VERSION, 'ZF Version', Zend_Wildfire_Plugin_FirePhp::INFO);
            $firephp->send(ceil($this->iPeakUsage/1000) .' p/kb', 'Память', Zend_Wildfire_Plugin_FirePhp::INFO);
            #$files = get_included_files();
            #$required = get_required_files();
            #$classes = get_declared_classes();
            #$firephp->send($classes, 'Классов (' . count($classes) . ')', Zend_Wildfire_Plugin_FirePhp::DUMP);
            #$firephp->send($files, 'Файлов INC (' . count($files) . ')', Zend_Wildfire_Plugin_FirePhp::DUMP);
            #$firephp->send($required, 'Файлов REQ (' . count($required) . ')', Zend_Wildfire_Plugin_FirePhp::DUMP);
            $firephp->send(sprintf("%01.4f", $iExecTime) . ' сек. (' . (($iExecDiff>0)?'+':'') . sprintf("%01.4f", $iExecDiff) .' сек.)', 'Генерация', Zend_Wildfire_Plugin_FirePhp::INFO);
            $firephp->send(
                array(
                    '<b>Куки:</b>' => $_COOKIE,
                    '<b>Сессии:</b>' => $_SESSION,
                    '<b>POST Параметры:</b>' => $_POST,
                    '<b>GET Параметры:</b>' => $_GET,
                    '<b>Zend Request:</b>' => $this->getRequest()->getParams()
                ), 
                Zend_Wildfire_Plugin_FirePhp::DUMP);

            $sql = new Zend_Wildfire_Plugin_FirePhp_TableMessage('SQL-Запросы');
            $sql->setBuffered(true);
            $sql->setHeader(array('Кол-во','Время','Среднее', 'За секунду'));
            $sql->setOption('includeLineNumbers', false);
            $sql->addRow(array((string)$queryCount, sprintf("%01.4f", $totalTime) . ' сек.', sprintf("%01.4f", $avarageQueryTime) . ' сек.', '~' . floor($queriesPerSecond)));
            $firephp->send($sql);
        }
    }
}

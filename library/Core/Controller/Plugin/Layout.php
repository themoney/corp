<?php

class Core_Controller_Plugin_Layout extends Zend_Controller_Plugin_Abstract
{

    /**
     * Called before Zend_Controller_Front enters its dispatch loop.
     *
     * @param  Zend_Controller_Request_Abstract $request
     * @return void
     */
    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
    {
        
        if ('foomodule' != $request->getModuleName()) {
            // If not in this module, return early
            return;
        }
    }

}